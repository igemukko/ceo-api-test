import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.remote.DesiredCapabilities

import static org.openqa.selenium.remote.DesiredCapabilities.firefox


driver = { new FirefoxDriver() }

environments {
//    'chrome' {
//        def chromeDriver = new File('src/test/resources/chromedriver')
//        System.setProperty('webdriver.chrome.driver' , chromeDriver.getAbsolutePath())
//        driver = { new ChromeDriver() }
//    }

    'firefox' {
        def firefoxDriver = new File('src/test/resources/geckodriver')
        System.setProperty('webdriver.gecko.driver', firefoxDriver.getAbsolutePath())

        driver = { new FirefoxDriver() }
    }
}

waiting {
    timeout = 6
    retryInterval = 0.5
    slow { timeout = 12 }
    reallySlow { timeout = 24 }
}
reportsDir = "target/geb-reports"