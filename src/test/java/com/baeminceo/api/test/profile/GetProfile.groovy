package com.baeminceo.api.test.profile

import com.baeminceo.api.test.utils.TestInit
import io.restassured.RestAssured
import io.restassured.http.ContentType
import io.restassured.path.json.JsonPath
import spock.lang.Unroll

import static com.baeminceo.api.test.utils.UtilMethod.generateStringFromResource
import static com.baeminceo.api.test.utils.UtilMethod.getJsonData
import static com.baeminceo.api.test.utils.UtilMethod.getJsonData
import static com.baeminceo.api.test.utils.UtilMethod.getMyCookies
import static io.restassured.RestAssured.given

class GetProfile extends TestInit {

    def setup() {
        RestAssured.basePath = "/v1/profiles/{memId}"
    }

    /*
    * 사용자의 프로필 정보를 조회한다.
    * 원 api: 헤더에 client info를 포함해서 api 요청
    * 수정 api: 업주/업소 정보가 변조되어 파라미터로 전송되었을 경우 front 에서 403 에러
    * 본 api는 url 파라미터로 전송하는 정보는 사장님 ID
    * 사장님 ID에 대한 유효성 체크는 front에서 처리해야 함
    */
    @Unroll
    def "[CEO] GET 사용자의 프로필 - #EXPLAIN" () {

        given: "헤더 정보 설정"
        requestHeader.put("Content-Type", ContentType.JSON)

        when:
        response = given()
                .log().all()
                .cookies(getMyCookies(LOGINID, LOGINPW))
                .headers(requestHeader)
                .pathParam("memId", MEMID)
                .get()

        response.prettyPrint()
        jsonPath = new JsonPath(response.getBody().asString())

        then:
        response.getStatusCode() == EXPECTRE1
        getJsonData("statusCode", jsonPath) == null ? EXPECTRE2 : getJsonData("statusCode", jsonPath).contains(EXPECTRE2) == true


        where:
        LOGINID     |   LOGINPW     |   MEMID               ||  EXPECTRE1  |   EXPECTRE2   |   EXPLAIN
        "lee00m"    |   "111111"    |   "lee00m"            ||  200        |   "SUCCESS"   |   "본인 ID"
//        "lee00m"    |   "111111"    |   "mooh05"            ||  403        |   "forbidden" |   "다른 업주 ID"
//        "lee00m"    |   "111111"    |   "testtestesttset"   ||  403        |   "forbidden" |   "없는 ID"
//        "lee00m"    |   "111111"    |   " "                 ||  403        |   "forbidden" |   "유효하지 않은 업주 ID(공백)"
//        "lee00m"    |   "111111"    |   "mooh05‘ group by userid having 1=1--"                ||  404        |   "forbidden" |   "SQL Injection"
//        "lee00m"    |   "111111"    |   "mooh05‘ SELECT name FROM syscolumns WHERE id = (SELECT id FROM sysobjects WHERE name = tablename’)--" ||  404        |   "forbidden" |   "SQL Injection"
//        "lee00m"    |   "111111"    |   "mooh05‘ OR ‘text’ > ‘t’"                             ||  404        |   "forbidden" |   "SQL Injection"
//        "lee00m"    |   "111111"    |   "mooh05‘ OR ‘whatever’ in (‘whatever’)"               ||  404        |   "forbidden" |   "SQL Injection"
//        "lee00m"    |   "111111"    |   "mooh05; EXECUTE IMMEDIATE ‘SEL’ || ‘ECT US’ || ‘ER’" ||  403        |   "forbidden" |   "SQL Injection"

    }
}
