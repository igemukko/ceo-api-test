package com.baeminceo.api.test.your

import com.baeminceo.api.test.utils.TestInit
import io.restassured.RestAssured
import io.restassured.http.ContentType
import io.restassured.path.json.JsonPath
import spock.lang.Unroll

import static com.baeminceo.api.test.utils.UtilMethod.generateStringFromResource
import static com.baeminceo.api.test.utils.UtilMethod.getJsonData
import static com.baeminceo.api.test.utils.UtilMethod.getMyCookies
import static io.restassured.RestAssured.given

class EditShopOwner extends TestInit {

    def setup() {
        RestAssured.basePath = "/v1/your/shop_owner"
    }

    /*
   * 광고중이 아닌 업주 정보를 수정한다.
   * 원 api: 헤더에 x-mem-no 및 x-mem-id를 포함해서 api 요청
   * 수정 api: 업주/업소 정보가 변조되어 파라미터로 전송되었을 경우 front 에서 403 에러
   * but 본 api는 파라미터로 업주 관련 정보만 설정하여 api 요청
   * 헤더에 사장님 ID 및 업주 번호가 없는 상태에서 api 요청 시 정상 처리되어야 함
   */
    @Unroll
    def "[CEO] EDIT 업주 정보 - #EXPLAIN"() {

        given: "json data의 업주 정보 설정"
        requestHeader.put("Content-Type", ContentType.JSON)
        requestBody = generateStringFromResource(JSONPATH)


        when:
        response = given()
                .log().all()
                .headers(requestHeader)
                .cookies(getMyCookies(LOGINID, LOGINPW))
                .body(requestBody)
                .post()

        response.prettyPrint()
        jsonPath = new JsonPath(response.getBody().asString())

        then:
        response.getStatusCode() == EXPECTRE1
        getJsonData("statusCode", jsonPath).contains(EXPECTRE2) == true

        where:
        LOGINID     |   LOGINPW         |    JSONPATH                       ||  EXPECTRE1   |   EXPECTRE2   |   EXPLAIN
//        "lee00m"    |   "111111"        |   "edit_shop_owner_info.json"     ||  403         |   "Forbidden" |   "광고등급 업주"
        "mooh05"    |   "a1111111"      |   "edit_shop_owner_info.json"     ||  200         |   "SUCCESS"   |   "일반등급 업주"

    }

}
