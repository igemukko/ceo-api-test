package com.baeminceo.api.test.your

import com.baeminceo.api.test.utils.TestInit
import io.restassured.RestAssured
import io.restassured.path.json.JsonPath
import spock.lang.Unroll

import static com.baeminceo.api.test.utils.UtilMethod.getJsonData
import static com.baeminceo.api.test.utils.UtilMethod.getMyCookies
import static io.restassured.RestAssured.given

class FindAllShopOPHour extends TestInit {

    def setup() {
        RestAssured.basePath = "/v1/your/all_shop_op_hour"
    }

    /*
    * 사용자의 업소 운영시간 목록을 가져온다.
    * 원 api: 헤더에 x-shopowner-no를 포함해서 api 요청
    * 수정 api: 업주/업소 정보가 변조되어 파라미터로 전송되었을 경우 front 에서 403 에러
    * but 본 api는 파라미터로 전송하는 것이 없음
    * 헤더에 사장님 ID 및 업주 번호가 없는 상태에서 api 요청 시 정상 처리되어야 함
    */
    @Unroll
    def "[CEO] FIND 사용자 전체업소의 운영시간 - #EXPLAIN"() {

        given: "설정 정보 없음"

        when:
        response = given()
                .log().all()
                .cookies(getMyCookies(LOGINID, LOGINPW))
                .get()

        response.prettyPrint()
        jsonPath = new JsonPath(response.getBody().asString())

        then:
        response.getStatusCode() == EXPECTRE1
        getJsonData("statusCode", jsonPath).contains(EXPECTRE2) == true

        where:
        LOGINID     |   LOGINPW     ||  EXPECTRE1   |   EXPECTRE2   |  EXPLAIN
        "lee00m"    |   "111111"    ||   200        |   "SUCCESS"   |  "요청 처리 성공"

    }
}
