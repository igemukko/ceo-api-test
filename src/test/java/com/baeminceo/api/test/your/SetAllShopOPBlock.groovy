package com.baeminceo.api.test.your

import com.baeminceo.api.test.utils.TestInit
import io.restassured.RestAssured
import io.restassured.http.ContentType
import io.restassured.path.json.JsonPath
import spock.lang.Unroll

import static com.baeminceo.api.test.utils.UtilMethod.generateStringFromResource
import static com.baeminceo.api.test.utils.UtilMethod.getJsonData
import static com.baeminceo.api.test.utils.UtilMethod.getMyCookies
import static io.restassured.RestAssured.given

class SetAllShopOPBlock extends TestInit {

    def setup() {
        RestAssured.basePath = "/v1/your/all_shop_op_block"
    }

    /*
    * 사용자의 전체 업소 운영 중지를 설정한다.
    * 원 api: 헤더에 x-shopowner-no 및 x-mem-id를 포함해서 api 요청
    * 수정 api: 업주/업소 정보가 변조되어 파라미터로 전송되었을 경우 front 에서 403 에러
    * but 본 api는 json data로 운영중지시작/종료시간, 사유, 코드만 전송함
    * 헤더에 사장님 ID 및 업주 번호가 없는 상태에서 api 요청 시 정상 처리되어야 함
    */
    @Unroll
    def "[CEO] SET 전체업소 운영중지- #EXPLAIN"() {

        given: "Content-Type 정보 설정"
        requestHeader.put("Content-Type", ContentType.JSON)
        requestBody = generateStringFromResource(JSONPATH)

        when:
        response = given()
                .log().all()
                .cookies(getMyCookies(LOGINID, LOGINPW))
                .headers(requestHeader)
                .body(requestBody)
                .post()

        response.prettyPrint()
        jsonPath = new JsonPath(response.getBody().asString())

        then:
        response.getStatusCode() == EXPECTRE1
        getJsonData("statusCode", jsonPath).contains(EXPECTRE2) == true

        where:
        LOGINID     |   LOGINPW     |   JSONPATH                        ||  EXPECTRE1   |   EXPECTRE2   |   EXPLAIN
        "lee00m"    |   "111111"    |   "set_all_shop_op_block.json"    ||  200         |   "SUCCESS"   |   "요청 처리 성공"

    }
}
