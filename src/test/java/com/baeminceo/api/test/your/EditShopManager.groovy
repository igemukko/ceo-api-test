package com.baeminceo.api.test.your

import com.baeminceo.api.test.utils.TestInit
import io.restassured.RestAssured
import io.restassured.http.ContentType
import io.restassured.path.json.JsonPath
import spock.lang.Unroll

import static com.baeminceo.api.test.utils.UtilMethod.generateStringFromResource
import static com.baeminceo.api.test.utils.UtilMethod.getJsonData
import static com.baeminceo.api.test.utils.UtilMethod.getMyCookies
import static io.restassured.RestAssured.given

class EditShopManager extends TestInit {

    def setup() {
        RestAssured.basePath = "/v1/your/shop_manager"
    }

    /*
    * 운영자 정보를 수정한다
    * 원 api: 헤더에 x-mem-no 및 x-mem-id를 포함해서 api 요청
    * 수정 api: 업주/업소 정보가 변조되어 파라미터로 전송되었을 경우 front 에서 403 에러
    * but 본 api는 파라미터로 전송하는 데이터는 운영자 정보와 관련된 정보임
    * 헤더에 사장님 ID 및 업주 번호가 없는 상태에서 api 요청 시 정상 처리되어야 함
    */
    @Unroll
    def "[CEO] EDIT 운영자 정보 - #EXPLAIN"() {

        given: "json data로 이루어진 운영 정보 설정"
        requestHeader.put("Content-Type", ContentType.JSON)
        requestBody = generateStringFromResource(JSONPATH)

        when:
        response = given()
                .log().all()
                .cookies(getMyCookies(LOGINID, LOGINPW))
                .headers(requestHeader)
                .body(requestBody)
                .post()

        response.prettyPrint()
        jsonPath = new JsonPath(response.getBody().asString())

        then:
        response.getStatusCode() == EXPECTRE1
        getJsonData("statusCode", jsonPath).contains(EXPECTRE2) == true

        where:
        LOGINID     |   LOGINPW     |   JSONPATH                    ||  EXPECTRE1   |   EXPECTRE2   |   EXPLAIN
        "lee00m"    |   "111111"    |   "edit_shop_manager.json"    ||  200         |   "SUCCESS"   |   "요청 처리 성공"

    }
}
