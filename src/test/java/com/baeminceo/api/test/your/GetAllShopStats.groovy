package com.baeminceo.api.test.your

import com.baeminceo.api.test.utils.TestInit
import io.restassured.RestAssured
import io.restassured.http.ContentType
import io.restassured.path.json.JsonPath
import spock.lang.Unroll

import static com.baeminceo.api.test.utils.UtilMethod.getJsonData
import static com.baeminceo.api.test.utils.UtilMethod.getMyCookies
import static io.restassured.RestAssured.given

class GetAllShopStats extends TestInit {

    def setup() {
        RestAssured.basePath = "/v1/your/all_shop_stats"
    }

    /*
    * 사용자의 업소 통계 정보를 가져온다.
    * 원 api: 헤더에 x-mem-no를 포함해서 api 요청
    * 수정 api: 업주/업소 정보가 변조되어 파라미터로 전송되었을 경우 front 에서 403 에러
    * but 본 api는 파라미터로 전송하는 것이 없음
    * 헤더에 사장님 ID 및 업주 번호가 없는 상태에서 api 요청 시 정상 처리되어야 함
    */
    @Unroll
    def "[CEO] GET 사용자의 업소통계 - #EXPLAIN"() {

        given: "Content-Type 정보 설정"
        requestHeader.put("Content-Type", ContentType.JSON)

        when:
        response = given()
                .log().all()
                .cookies(getMyCookies(LOGINID, LOGINPW))
                .get()

        response.prettyPrint()
        jsonPath = new JsonPath(response.getBody().asString())

        then:
        response.getStatusCode() == EXPECTRE1
        getJsonData("statusCode", jsonPath).contains(EXPECTRE2) == true

        where:
        LOGINID     |   LOGINPW     ||  EXPECTRE1   |   EXPECTRE2   |  EXPLAIN
        "lee00m"    |   "111111"    ||   200        |   "SUCCESS"   |  "요청 처리 성공"

    }
}
