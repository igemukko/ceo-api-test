package com.baeminceo.api.test.shop

import com.baeminceo.api.test.utils.TestInit
import io.restassured.RestAssured
import io.restassured.path.json.JsonPath
import spock.lang.Unroll

import static com.baeminceo.api.test.utils.UtilMethod.getJsonData
import static com.baeminceo.api.test.utils.UtilMethod.getJsonData
import static com.baeminceo.api.test.utils.UtilMethod.getMyCookies
import static io.restassured.RestAssured.given

class FindShopsByCondition extends TestInit {

    def setup() {
        RestAssured.basePath = "/v1/shops"
    }

    /*
    * 업소가 현재 이용중인 광고상품 목록을 제공합니다. 적용기간이 지난 광고상품은 조회대상에서 제외됩니다.
    * 원 api: 헤더에 client info를 포함해서 api 요청
    * 수정 api: 업주/업소 정보가 변조되어 파라미터로 전송되었을 경우 front 에서 403 에러
    * 본 api는 url 쿼리 파라미터로 전송하는 정보는 업주번호임
    * 업주 번호에 대한 유효성 체크는 front에서 처리해야 함
    */
    @Unroll
    def "[CEO] FIND 업소 by 검색조건 - #EXPLAIN" () {

        given: "설정 정보 없음"

        when:
        response = given()
                .log().all()
                .cookies(getMyCookies(LOGINID, LOGINPW))
                .queryParam("shopOwnerNo", SHOPOWNERNO)
                .get()

        response.prettyPrint()
        jsonPath = new JsonPath(response.getBody().asString())

        then:
        response.getStatusCode() == EXPECTRE1
        getJsonData("statusCode", jsonPath) == null ? EXPECTRE2 : getJsonData("statusCode", jsonPath).contains(EXPECTRE2) == true


        where:
        LOGINID     |   LOGINPW     |   SHOPOWNERNO                                     ||  EXPECTRE1  |   EXPECTRE2   |   EXPLAIN
        "lee00m"    |   "111111"    |   "201203220076"                                  ||  200        |   "SUCCESS"   |   "로그인한 업주"
        "lee00m"    |   "111111"    |   "201709140008"                                  ||  403        |   "forbidden" |   "다른 업주"
        "lee00m"    |   "111111"    |   "000000000000"                                  ||  403        |   "forbidden" |   "없는 업주"
        "lee00m"    |   "111111"    |   " "                                             ||  403        |   "forbidden" |   "유효하지 않은 업소 업주(공백)"
        'lee00m'    |   "111111"    |   "abcedfghijklmnopqrstuvwxyz12345678"            ||  403        |   "forbidden" |   "유효하지 않은 업소 업주(문자+숫자)"
        'lee00m'    |   "111111"    |   "201709140008%22+or+isnull%281%2F0%29+%2F* "    ||  403        |   "forbidden" |   "sql injection"
        'lee00m'    |   "111111"    |   " 201709140008%27+--+"                          ||  403        |   "forbidden" |   "sql injection"
        'lee00m'    |   "111111"    |   "201709140008‘ or 1=1-- "                       ||  403        |   "forbidden" |   "sql injection"
        'lee00m'    |   "111111"    |   "201709140008“ or 1=1--"                        ||  403        |   "forbidden" |   "sql injection"
        'lee00m'    |   "111111"    |   "201709140008'or1=1/* "                         ||  403        |   "forbidden" |   "sql injection"



    }

}
