package com.baeminceo.api.test.shop

import com.baeminceo.api.test.utils.TestInit
import io.restassured.RestAssured
import io.restassured.http.ContentType
import io.restassured.path.json.JsonPath
import spock.lang.Unroll

import static com.baeminceo.api.test.utils.UtilMethod.generateStringFromResource
import static com.baeminceo.api.test.utils.UtilMethod.getJsonData
import static com.baeminceo.api.test.utils.UtilMethod.getJsonData
import static com.baeminceo.api.test.utils.UtilMethod.getMyCookies
import static io.restassured.RestAssured.given

class EditShopInfo extends TestInit {

    def setup() {
        RestAssured.basePath = "/v1/shops/{shopNo}/info"
    }

    /*
    * 업소(shopNo) 기본정보를 수정한다.
    * 원 api: 헤더에 client info를 포함해서 api 요청
    * 수정 api: 업주/업소 정보가 변조되어 파라미터로 전송되었을 경우 front 에서 403 에러
    * 본 api는 url 파라미터로 전송하는 정보는 업소번호임
    * 소유 업소에 대한 유효성 체크는 front에서 처리해야 함
    */
    @Unroll
    def "[CEO] EDIT 업소 기본정보- #EXPLAIN" () {

        given: "헤더 및 body 정보 설정"
        requestHeader.put("Content-Type", ContentType.JSON)

        requestBody = generateStringFromResource(JSONPATH)

        when:
        response = given()
                .log().all()
                .cookies(getMyCookies(LOGINID, LOGINPW))
                .headers(requestHeader)
                .pathParam("shopNo", SHOPNO)
                .body(requestBody)
                .post()

        response.prettyPrint()
        jsonPath = new JsonPath(response.getBody().asString())

        then:
        response.getStatusCode() == EXPECTRE1
        getJsonData("statusCode", jsonPath) == null ? EXPECTRE2 : getJsonData("statusCode", jsonPath).contains(EXPECTRE2) == true


        where:
        LOGINID     |   LOGINPW     |   SHOPNO                              |   JSONPATH                                   ||  EXPECTRE1  |   EXPECTRE2   |   EXPLAIN
        "lee00m"    |   "111111"    |   "445078"                            |   "edit_shop_info.json"                      ||  200        |   "SUCCESS"   |   "소유 업소"
        "lee00m"    |   "111111"    |   "658428"                            |   "edit_shop_info.json"                      ||  403        |   "forbidden" |   "미소유 업소"
        "lee00m"    |   "111111"    |   "000000"                            |   "edit_shop_info.json"                      ||  403        |   "forbidden" |   "없는 업소"
        "lee00m"    |   "111111"    |   " "                                 |   "edit_shop_info.json"                      ||  404        |   true        |   "유효하지 않은 업소 조회(공백)"
        'lee00m'    |   "111111"    |   "658428‘ and 1 in (select var from temp)--"|   "edit_shop_info.json"               ||  404        |   true        |   "SQL Ijection"
        'lee00m'    |   "111111"    |   "658428‘) or (‘a’=’a"                      |   "edit_shop_info.json"               ||  404        |   true        |   "SQL Ijection"
        'lee00m'    |   "111111"    |   "658428‘ OR ‘unusual’ = ‘unusual’"         |   "edit_shop_info.json"               ||  404        |   true        |   "SQL Ijection"
        'lee00m'    |   "111111"    |   "658428‘ OR ‘something’ = ‘some’+’thing’"  |   "edit_shop_info.json"               ||  404        |   true        |   "SQL Ijection"
        'lee00m'    |   "111111"    |   "658428‘ OR ‘text’ = N’text’"              |   "edit_shop_info.json"               ||  404        |   true        |   "SQL Ijection"
        'lee00m'    |   "111111"    |   "658428‘ OR ‘something’ like ‘some%’"      |   "edit_shop_info.json"               ||  404        |   true        |   "SQL Ijection"

    }

}
