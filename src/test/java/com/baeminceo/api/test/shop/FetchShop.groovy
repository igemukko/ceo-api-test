package com.baeminceo.api.test.shop

import com.baeminceo.api.test.utils.TestInit
import io.restassured.RestAssured
import io.restassured.http.ContentType
import io.restassured.path.json.JsonPath
import spock.lang.Unroll

import static com.baeminceo.api.test.utils.UtilMethod.getMyCookies
import static io.restassured.RestAssured.given

class FetchShop extends TestInit {

    def setup() {
        RestAssured.basePath = "/v1/shops/fetch_by_members"
    }

    /*
    * 사용하지 않음
    * 업주가 하나 이상의 업소를 가질 경우 카테고리 관계없이 최근일 기준 기준업소 1개만 가져온다.
    * 원 api: 헤더에 client info를 포함해서 api 요청
    * 수정 api: 업주/업소 정보가 변조되어 파라미터로 전송되었을 경우 front 에서 403 에러
    * 본 api는 파라미터로 전송하는 정보는 업주의 회원번호들을 포함시킴
    * 업주의 회원번호가 맞는지 여부에 대해 유효성 처리해야 함
    */
    @Unroll
    def "[CEO] Fetch 업소 - #EXPLAIN" () {
        given: "설정 정보 없음"
        requestHeader.put("Content-Type", ContentType.JSON)

        when:
        response = given()
                .log().all()
                .cookies(getMyCookies(LOGINID, LOGINPW))
                .pathParam("shopNo", SHOPNO)
                .get()

        response.prettyPrint()
        jsonPath = new JsonPath(response.getBody().asString())

        then:
        response.getStatusCode() == 403
//        getJsonData("statusCode", jsonPath).contains("Invalid") == true

        where:
        LOGINID     |   LOGINPW     |   SHOPNO                              ||  EXPLAIN
        "lee00m"    |   "111111"    |   "445078"                            ||  "소유 리뷰"
        "lee00m"    |   "111111"    |   "658428"                            ||  "미소유 리뷰"
        "lee00m"    |   "111111"    |   " "                                 ||  "유효하지 않은 업소 조회(공백)"
        'lee00m'    |   "111111"    |   "abcedfghijklmnopqrstuvwxyz12345678"||  "유효하지 않은 업소 조회(문자+숫자)"

    }
}
