package com.baeminceo.api.test.shop

import com.baeminceo.api.test.utils.TestInit
import io.restassured.RestAssured
import io.restassured.http.ContentType
import io.restassured.path.json.JsonPath
import spock.lang.Unroll

import static com.baeminceo.api.test.utils.UtilMethod.generateStringFromResource
import static com.baeminceo.api.test.utils.UtilMethod.getJsonData
import static com.baeminceo.api.test.utils.UtilMethod.getJsonData
import static com.baeminceo.api.test.utils.UtilMethod.getMyCookies
import static io.restassured.RestAssured.given

class GetMenuAPIInfo extends TestInit {

    def setup() {
        RestAssured.basePath = "/v1/shops/{shopNo}/menuapi_info"
    }

    /*
    * 사용자의 업소의 메뉴 API 정보를 조회한다. (메뉴판이 있을 경우 true, 없으면 false)
    * 원 api: 헤더에 client info를 포함해서 api 요청
    * 수정 api: 업주/업소 정보가 변조되어 파라미터로 전송되었을 경우 front 에서 403 에러
    * 본 api는 url 파라미터로 전송하는 정보는 업소 번호를 포함시킴
    * 소유 업소 여부에 대해 유효성은 front에서 처리 해야 함
    */
    @Unroll
    def "[CEO] GET 메뉴API 정보 - #EXPLAIN" () {

        given: "설정 정보 없음"

        when:
        response = given()
                .log().all()
                .cookies(getMyCookies(LOGINID, LOGINPW))
                .pathParam("shopNo", SHOPNO)
                .get()

        response.prettyPrint()
        jsonPath = new JsonPath(response.getBody().asString())

        then:
        response.getStatusCode() == EXPECTRE1
        getJsonData("statusCode", jsonPath) == null ? EXPECTRE2 : getJsonData("statusCode", jsonPath).contains(EXPECTRE2) == true


        where:
        LOGINID     |   LOGINPW     |   SHOPNO                                                                                                  ||  EXPECTRE1   |   EXPECTRE2   |   EXPLAIN
        "lee00m"    |   "111111"    |   "445078"                                                                                                ||  200         |   "SUCCESS"   |   "소유 업소"
        "lee00m"    |   "111111"    |   "658428"                                                                                                ||  403         |   "forbidden" |   "미소유 업소"
        "lee00m"    |   "111111"    |   "000000"                                                                                                ||  403         |   "forbidden" |   "없는 업소"
        "lee00m"    |   "111111"    |   " "                                                                                                     ||  404         |   true        |   "않은 업소 조회(공백)"
        'lee00m'    |   "111111"    |   "abcedfghijklmnopqrstuvwxyz12345678"                                                                    ||  404         |   true        |   "유효하지 않은 업소 조회(문자+숫자)"
        'lee00m'    |   "111111"    |   "658428) UNION SELECT%20*%20FROM%20INFORMATION_SCHEMA. TABLES;"                                         ||  403         |   "forbidden"        |   "sql injection"
        'lee00m'    |   "111111"    |   "658428‘ having 1=1-- "                                                                                  ||  403         |   "forbidden"        |   "sql injection"
        'lee00m'    |   "111111"    |   "658428‘ group by userid having 1=1--"                                                                  ||  403         |   "forbidden"        |   "sql injection"
        'lee00m'    |   "111111"    |   "445078‘ SELECT name FROM syscolumns WHERE id = (SELECT id FROM sysobjects WHERE name = tablename’)--"  ||  403         |   "forbidden"        |   "sql injection"
        'lee00m'    |   "111111"    |   "658428‘ or 1 in (select @@version)--"                                                                  ||  403         |   "forbidden"        |   "sql injection"

    }
}
