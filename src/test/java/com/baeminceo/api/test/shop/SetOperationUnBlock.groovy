package com.baeminceo.api.test.shop

import com.baeminceo.api.test.utils.TestInit
import io.restassured.RestAssured
import io.restassured.http.ContentType
import io.restassured.path.json.JsonPath
import spock.lang.Unroll

import static com.baeminceo.api.test.utils.UtilMethod.generateStringFromResource
import static com.baeminceo.api.test.utils.UtilMethod.getJsonData
import static com.baeminceo.api.test.utils.UtilMethod.getMyCookies
import static com.baeminceo.api.test.utils.UtilMethod.updateToResource
import static io.restassured.RestAssured.given

class SetOperationUnBlock extends TestInit {

    def setup() {
        RestAssured.basePath = "/v1/shops/{shopNo}/operation_unblock"
    }

    /*
    * 사용자의 업소의 운영중지를 해제한다.
    * 원 api: 헤더에 x-mem-no를 포함해서 api 요청
    * 수정 api: 업주/업소 정보가 변조되어 파라미터로 전송되었을 경우 front 에서 403 에러
    * 본 api는 url 파라미터로 전송하는 정보는 업소 번호임
    * 소유 업소 여부에 대해 유효성 처리는 front에서 처리해야 함
    */
    @Unroll
    def "[CEO] SET 운영중지해제 - #EXPLAIN" () {

        given: "헤더 및 body 정보 설정"
        requestHeader.put("Content-Type", ContentType.JSON)
        requestBody = generateStringFromResource(JSONPATH)

        when:
        response = given()
                .log().all()
                .cookies(getMyCookies(LOGINID, LOGINPW))
                .headers(requestHeader)
                .pathParam("shopNo", SHOPNO)
                .body(requestBody)
                .post()

        response.prettyPrint()
        jsonPath = new JsonPath(response.getBody().asString())

        then:
        response.getStatusCode() == EXPECTRE1
        getJsonData("statusCode", jsonPath) == null ? EXPECTRE2 : getJsonData("statusCode", jsonPath).contains(EXPECTRE2) == true


        where:
        LOGINID     |   LOGINPW     |   JSONPATH                     |   SHOPNO                                 ||  EXPECTRE1  |   EXPECTRE2   |   EXPLAIN
        "lee00m"    |   "111111"    |   "set_shop_op_unblock.json"   |   "445078"                               ||  200        |   "SUCCESS"   |   "소유 업소"
        "lee00m"    |   "111111"    |   "set_shop_op_unblock.json"   |   "658428"                               ||  403        |   "forbidden" |   "미소유 업소"
        "lee00m"    |   "111111"    |   "set_shop_op_unblock.json"   |   "000000"                               ||  403        |   "forbidden" |   "없는 업소"
        "lee00m"    |   "111111"    |   "set_shop_op_unblock.json"   |   " "                                    ||  404        |   true        |   "유효하지 않은 업소 조회(공백)"
        'lee00m'    |   "111111"    |   "set_shop_op_unblock.json"   |   "abcedfghijklmnopqrstuvwxyz12345678"   ||  404        |   true        |   "유효하지 않은 업소 조회(문자+숫자)"
        'lee00m'    |   "111111"    |   "set_shop_op_unblock.json"   |   "658428%22+or+isnull%281%2F0%29+%2F*"  ||  403        |   "forbidden"        |   "sql injection"
        'lee00m'    |   "111111"    |   "set_shop_op_unblock.json"   |   "658428%27+--+"                        ||  403        |   "forbidden"        |   "sql injection"
        'lee00m'    |   "111111"    |   "set_shop_op_unblock.json"   |   "658428‘ or 1=1--"                     ||  403        |   "forbidden"        |   "sql injection"
        'lee00m'    |   "111111"    |   "set_shop_op_unblock.json"   |   "658428“ or 1=1--"                     ||  403        |   "forbidden"        |   "sql injection"
        'lee00m'    |   "111111"    |   "set_shop_op_unblock.json"   |   "658428‘or1=1/*"                       ||  403        |   "forbidden"        |   "sql injection"
        'lee00m'    |   "111111"    |   "set_shop_op_unblock.json"   |   "658428 or 1=1--"                      ||  403        |   "forbidden"        |   "sql injection"

    }
}
