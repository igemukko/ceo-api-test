package com.baeminceo.api.test.shop

import com.baeminceo.api.test.utils.TestInit
import io.restassured.RestAssured
import io.restassured.http.ContentType
import io.restassured.path.json.JsonPath
import spock.lang.Unroll

import static com.baeminceo.api.test.utils.UtilMethod.generateStringFromResource
import static com.baeminceo.api.test.utils.UtilMethod.getJsonData
import static com.baeminceo.api.test.utils.UtilMethod.getJsonData
import static com.baeminceo.api.test.utils.UtilMethod.getMyCookies
import static io.restassured.RestAssured.given

class EditShopOPInfo extends TestInit {

    def setup() {
        RestAssured.basePath = "/v1/shops/{shopNo}/operation_info"
    }

    /*
    * 업소(shopNo) 운영정보를 수정한다.
    * 원 api: 헤더에 client info 및 업주 정보를 포함해서 api 요청
    * 수정 api: 업주/업소 정보가 변조되어 파라미터로 전송되었을 경우 front 에서 403 에러
    * 본 api는 url 파라미터로 전송하는 정보는 업소번호임
    * 소유 업소에 대한 유효성 체크는 front에서 처리해야 함
    */
    @Unroll
    def "[CEO] EDIT 업소 운영정보 - #EXPLAIN" () {

        given: "운영 정보 설정"
        requestHeader.put("Content-Type", ContentType.JSON)

        requestBody = generateStringFromResource(JSONPATH)

        when:
        response = given()
                .log().all()
                .cookies(getMyCookies(LOGINID, LOGINPW))
                .headers(requestHeader)
                .pathParam("shopNo", SHOPNO)
                .body(requestBody)
                .post()

        response.prettyPrint()
        jsonPath = new JsonPath(response.getBody().asString())

        then:
        response.getStatusCode() == EXPECTRE1
        getJsonData("statusCode", jsonPath) == null ? EXPECTRE2 : getJsonData("statusCode", jsonPath).contains(EXPECTRE2) == true


        where:
        LOGINID     |   LOGINPW     |   SHOPNO                              |   JSONPATH                    ||  EXPECTRE1  |   EXPECTRE2   |   EXPLAIN
        "lee00m"    |   "111111"    |   "445078"                            |   "edit_shop_op_info.json"    ||  200        |   "SUCCESS"   |   "소유 업소"
        "lee00m"    |   "111111"    |   "658428"                            |   "edit_shop_op_info.json"    ||  403        |   "forbidden" |   "미소유 업소"
        "lee00m"    |   "111111"    |   "000000"                            |   "edit_shop_op_info.json"    ||  403        |   "forbidden" |   "없는 업소"
        "lee00m"    |   "111111"    |   " "                                 |   "edit_shop_op_info.json"    ||  404        |   true        |   "유효하지 않은 업소 조회(공백)"
        'lee00m'    |   "111111"    |   "abcedfghijklmnopqrstuvwxyz12345678"|   "edit_shop_op_info.json"    ||  404        |   true        |   "유효하지 않은 업소 조회(문자+숫자)"
        "lee00m"    |   "111111"    |   "658428‘ having 1=1--"                |   "edit_shop_op_info.json"    ||  403        |   "SUCCESS"   |   "소유 업소"
        "lee00m"    |   "111111"    |   "658428‘ having 1=1--"                |   "edit_shop_op_info.json"    ||  403        |   "SUCCESS"   |   "소유 업소"
        "lee00m"    |   "111111"    |   "658428‘ group by userid having 1=1--"|   "edit_shop_op_info.json"    ||  403        |   "SUCCESS"   |   "소유 업소"
        "lee00m"    |   "111111"    |   "658428‘or1/*"                        |   "edit_shop_op_info.json"    ||  403        |   "SUCCESS"   |   "소유 업소"
        "lee00m"    |   "111111"    |   "658428(||6)"                         |   "edit_shop_op_info.json"    ||  403        |   "SUCCESS"   |   "소유 업소"

    }
}
