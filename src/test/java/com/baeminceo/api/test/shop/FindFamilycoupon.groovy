package com.baeminceo.api.test.shop

import com.baeminceo.api.test.utils.TestInit
import io.restassured.RestAssured
import io.restassured.path.json.JsonPath
import spock.lang.Unroll

import static com.baeminceo.api.test.utils.UtilMethod.getJsonData
import static com.baeminceo.api.test.utils.UtilMethod.getJsonData
import static com.baeminceo.api.test.utils.UtilMethod.getMyCookies
import static io.restassured.RestAssured.given

class FindFamilycoupon extends TestInit {

    def setup() {
        RestAssured.basePath = "/v1/shops/{shopNo}/familycoupon"
    }

    /*
    * 업소단골쿠폰조회. 회원번호 필터 조건을 넣으면 해당 회원에게 발급 가능한 쿠폰만 조회됩니다.
    * 원 api: 헤더에 client info를 포함해서 api 요청
    * 수정 api: 업주/업소 정보가 변조되어 파라미터로 전송되었을 경우 front 에서 403 에러
    * 본 api는 url 파라미터로 전송하는 정보는 업소 및 회원번호임
    * 소유 업소에 대한 유효성 체크는 front에서 처리해야 함
    */
    @Unroll
    def "[CEO] FIND 업소 단골쿠폰 - #EXPLAIN" () {

        given: "설정 정보 없음"

        when:
        response = given()
                .log().all()
                .cookies(getMyCookies(LOGINID, LOGINPW))
                .pathParam("shopNo", SHOPNO)
                .queryParams("memNo", MEMNO)
                .get()

        response.prettyPrint()
        jsonPath = new JsonPath(response.getBody().asString())

        then:
        response.getStatusCode() == EXPECTRE1
        getJsonData("statusCode", jsonPath) == null ? EXPECTRE2 : getJsonData("statusCode", jsonPath).contains(EXPECTRE2) == true


        where:
        LOGINID     |   LOGINPW     |   SHOPNO                                      |   MEMNO                                                               ||  EXPECTRE1  |   EXPECTRE2   |   EXPLAIN
        "lee00m"    |   "111111"    |   "445078"                                    |   "201110018977"                                                      ||  200        |   "SUCCESS"   |   "소유 업소"
        "lee00m"    |   "111111"    |   "658428"                                    |   "201110018977"                                                      ||  403        |   "forbidden" |   "미소유 업소"
        "lee00m"    |   "111111"    |   "000000"                                    |   "201110018977"                                                      ||  403        |   "forbidden" |   "없는 업소"
        "lee00m"    |   "111111"    |   " "                                         |   "201110018977"                                                      ||  404        |   true        |   "유효하지 않은 업소 조회(공백)"
        'lee00m'    |   "111111"    |   "abcedfghijklmnopqrstuvwxyz12345678"        |   "201110018977"                                                      ||  404        |   true        |   "유효하지 않은 업소 조회(문자+숫자)"
        "lee00m"    |   "111111"    |   "445078"                                    |   "000000000000"                                                      ||  400        |   "Invalid"   |   "없는 사용자"
        "lee00m"    |   "111111"    |   "445078"                                    |   " "                                                                 ||  400        |   "Invalid"   |   "유효하지 않은 사용자(공백)"
        "lee00m"    |   "111111"    |   "445078"                                    |   "abcedfghijklmnopqrstuvwxyz12345678"                                ||  400        |   "Invalid"   |   "유효하지 않은 사용자(문자+숫자)"
        "lee00m"    |   "111111"    |   "445078"                                    |   "170309000009"                                                      ||  400        |   "Invalid"   |   "리뷰를 등록하지 않은 사용자"
        "lee00m"    |   "111111"    |   "658428‘ OR 2 > 1"                          |   "201110018977"                                                      ||  403        |   "forbidden"   |   "sql injection"
        "lee00m"    |   "111111"    |   "658428‘ OR ‘text’ > ‘t’"                   |   "201110018977"                                                      ||  403        |   "forbidden"   |   "sql injection"
        "lee00m"    |   "111111"    |   "658428‘ OR ‘whatever’ in (‘whatever’) "     |   "201110018977"                                                      ||  403        |   "forbidden"   |   "sql injection"
        "lee00m"    |   "111111"    |   "658428‘ OR 2 BETWEEN 1 and 3"              |   "201110018977"                                                      ||  403        |   "forbidden"   |   "sql injection"
        "lee00m"    |   "111111"    |   "‘ or username like char(37);"              |   "201110018977"                                                      ||  404        |   true          |   "sql injection"
        "lee00m"    |   "111111"    |   "445078"                                    |   "‘ union select * from users where login = char(114,111,111,116);"  ||  400        |   "Invalid"   |   "sql injection"
        "lee00m"    |   "111111"    |   "445078"                                    |   "170309000009‘ union select "                                        ||  400        |   "Invalid"   |   "sql injection"
        "lee00m"    |   "111111"    |   "445078"                                    |   "Password:*/=1--"                                                   ||  400        |   "Invalid"   |   "sql injection"
        "lee00m"    |   "111111"    |   "445078"                                    |   "UNI/**/ON SEL/**/ECT"                                              ||  400        |   "Invalid"   |   "sql injection"
        "lee00m"    |   "111111"    |   "445078"                                    |   "170309000009‘; EXECUTE IMMEDIATE ‘SEL’ || ‘ECT US’ || ‘ER’"        ||  400        |   "Invalid"   |   "sql injection"
        "lee00m"    |   "111111"    |   "445078"                                    |   "‘ OR ‘1’=’1’"                                          ||  400        |   "Invalid"   |   "sql injection"

    }
    
}
