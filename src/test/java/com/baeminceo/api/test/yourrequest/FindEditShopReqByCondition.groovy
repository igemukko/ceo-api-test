package com.baeminceo.api.test.yourrequest

import com.baeminceo.api.test.utils.TestInit
import io.restassured.RestAssured
import io.restassured.http.ContentType
import io.restassured.path.json.JsonPath
import io.restassured.response.Response
import spock.lang.Unroll

import static com.baeminceo.api.test.utils.UtilMethod.getJsonData
import static com.baeminceo.api.test.utils.UtilMethod.getMyCookies
import static io.restassured.RestAssured.given

class FindEditShopReqByCondition extends TestInit {

    def setup() {
        RestAssured.basePath = "/v1/your_reqs/edit_shop/"
    }

    /*
    * 사용자의 전체 업소 수정요청내역을 요청번호로 조회한다
    * 원 api: 헤더에 x-mem-no를 포함해서 api 요청
    * 수정 api: 업주/업소 정보가 변조되어 파라미터로 전송되었을 경우 front 에서 403 에러
    * 업주/업소 정보를 제외한 파라미터 유효성: B2B 서버에서 체크
    */
    @Unroll
    def "[CEO] FIND 업소정보수정요청 by ID - #EXPLAIN"() {

        given: "설정 정보 없음"
        requestHeader.put("Content-Type", ContentType.JSON)

        when:
        response = given()
                .log().all()
                .cookies(getMyCookies(LOGINID, LOGINPW))
                .headers(requestHeader)
                .get(SHOPCLAIMSEQ)

        response.prettyPrint()
        jsonPath = new JsonPath(response.getBody().asString())

        then:
        response.getStatusCode() == EXPECTRE1
        getJsonData("statusCode", jsonPath) == null ? EXPECTRE2 : getJsonData("statusCode", jsonPath).contains(EXPECTRE2) == true

        where:
        LOGINID     |   LOGINPW     |   SHOPCLAIMSEQ                                            ||  EXPECTRE1    |  EXPECTRE2       |   EXPLAIN
        "lee00m"    |   "111111"    |   "216177"                                               ||  200          |  "SUCCESS"       |   "정상 요청 번호"
        "lee00m"    |   "111111"    |   "216178"                                               ||  400          |  "Invalid"       |   "미소유 요청번호"
        "lee00m"    |   "111111"    |   "000000"                                               ||  404          |  "Not"           |   "존재하지 않는 요청 번호"
        "lee00m"    |   "111111"    |   "abcdefghijklnmopqrstuvwxyz0000000000"                 ||  404          |  true            |   "비정상 요청 번호(문자+숫자)"
        "lee00m"    |   "111111"    |   "OR 1=1"                                                ||  404          |  true            |   "sql injection"
        "lee00m"    |   "111111"    |   "‘ OR ‘1’=’1"                                           ||  404          |  true            |   "sql injection"
        "lee00m"    |   "111111"    |   "; OR ‘1’=’1’"                                            ||  400          |  "Invalid"       |   "sql injection"
        "lee00m"    |   "111111"    |   "216178%22+or+isnull%281%2F0%29+%2F*"                   ||  404          |  true            |   "sql injection"
        "lee00m"    |   "111111"    |   "216178%27+OR+%277659%27%3D%277659"                     ||  404          |  true            |   "sql injection"
    }

    /*
    * 사용자의 전체업소 수정요청내역을 조회한다.
    * 원 api: 헤더에 x-shopowner-no를 포함해서 api 요청
    * 수정 api: 업주/업소 정보가 변조되어 파라미터로 전송되었을 경우 front 에서 403 에러
    * 업주/업소 정보를 제외한 파라미터 유효성: B2B 서버에서 체크
    * but 본 api는 파라미터로 전송하는 데이터가 없으므로 헤더에 업주 번호 없이 요청 시 정상 처리되어야 함
    */
    @Unroll
    def "[CEO] FIND 업소정보수정요청 By 검색조건 - #EXPLAIN"() {

        given: "설정 정보 없음"
        requestHeader.put("Content-Type", ContentType.JSON)

        when:
        response = given()
                .log().all()
                .cookies(getMyCookies(LOGINID, LOGINPW))
                .headers(requestHeader)
                .get()

        response.prettyPrint()
        jsonPath = new JsonPath(response.getBody().asString())

        then:
        response.getStatusCode() == EXPECTRE1
        getJsonData("statusCode", jsonPath).contains(EXPECTRE2) == true

        where:
        LOGINID     |   LOGINPW     ||  EXPECTRE1   |   EXPECTRE2   |   EXPLAIN
        "lee00m"    |   "111111"    ||  200         |   "SUCCESS"   |   "정상 요청"

    }
}
