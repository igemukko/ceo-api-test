package com.baeminceo.api.test.yourrequest

import com.baeminceo.api.test.utils.TestInit
import io.restassured.RestAssured
import io.restassured.http.ContentType
import io.restassured.path.json.JsonPath
import spock.lang.Unroll

import static com.baeminceo.api.test.utils.UtilMethod.generateStringFromResource
import static com.baeminceo.api.test.utils.UtilMethod.getJsonData
import static com.baeminceo.api.test.utils.UtilMethod.getMyCookies
import static com.baeminceo.api.test.utils.UtilMethod.updateToResource
import static io.restassured.RestAssured.given


class REQEditShop extends TestInit {

    def setup() {
        RestAssured.basePath = "v1/your_reqs/edit_shop"
    }

    /*
    * 사용자가 업소정보수정요청을 보낸다.
    * 원 api: 헤더에 x-mem-id 및 x-mem-no을 포함해서 api 요청, 파라미터로 업소 번호
    * 수정 api: 업주/업소 정보가 변조되어 url 파라미터로 전송되었을 경우 front 에서 403 에러
    * 수정 api 파라미터: 업소번호/요청카테고리/요청내용/첨부 이미지 경로/첨부 이미지 이름
    * 유효성을 체크해야 하는 업소번호는 json data 형태 : B2B 서버에서 체크해야
    */
    @Unroll
    def "[CEO] REQ 업소정보수정요청 - #EXPLAIN" () {

        given:
        requestHeader.put("Content-Type", ContentType.JSON)

        updateToResource(JSONPATH, "shopNo", SHOPNO)
        requestBody = generateStringFromResource(JSONPATH)

        when:
        response = given()
                .log().all()
                .cookies(getMyCookies(LOGINID, LOGINPW))
                .headers(requestHeader)
                .body(requestBody)
                .post()

        response.prettyPrint()
        jsonPath = new JsonPath(response.getBody().asString())

        then:
        response.getStatusCode() == EXPECTRE1
        getJsonData("statusCode", jsonPath).contains(EXPECTRE2) == true

        where:
        LOGINID     |   LOGINPW     |  JSONPATH                     |   SHOPNO                                          ||  EXPECTRE1   |   EXPECTRE2   |  EXPLAIN
        "lee00m"    |   "111111"    |  "req_edit_shop_info.json"    |   "445078"                                        ||  200         |   "SUCCESS"   |  "소유 업소"          //SUCCESS
        "lee00m"    |   "111111"    |  "req_edit_shop_info.json"    |   "658428"                                        ||  400         |   "Invalid"   |  "미소유 업소"        //Invalidparameter
        "lee00m"    |   "111111"    |  "req_edit_shop_info.json"    |   null                                            ||  400         |   "Invalid"   |  "없소번호 null"     //InvalidParameter
        "lee00m"    |   "111111"    |  "req_edit_shop_info.json"    |   "000000"                                        ||  400         |   "Invalid"   |  "없는 업소번호"      //InvalidParameter
        "lee00m"    |   "111111"    |  "req_edit_shop_info.json"    |   "qwertyuiopasdfghjklzxcvbnm1234567890"          ||  400         |   "Invalid"   |  "비정상 업소번호"     //InvalidParameter
        "lee00m"    |   "111111"    |  "req_edit_shop_info.json"    |   "658428‘||(elt(-3+5,bin(15),ord(10),hex(char(45))))"  ||  403         |   "forbidden"   |  "비정상 업소번호"
        "lee00m"    |   "111111"    |  "req_edit_shop_info.json"    |   "658428||6"                                           ||  403         |   "forbidden"   |  "비정상 업소번호"
        "lee00m"    |   "111111"    |  "req_edit_shop_info.json"    |   "658428‘||’6"                                         ||  403         |   "forbidden"   |  "비정상 업소번호"
        "lee00m"    |   "111111"    |  "req_edit_shop_info.json"    |   "658428(||6)"                                         ||  403         |   "forbidden"   |  "비정상 업소번호"
        "lee00m"    |   "111111"    |  "req_edit_shop_info.json"    |   "658428‘ OR 1=1--"                                    ||  403         |   "forbidden"   |  "비정상 업소번호"
    }

}
