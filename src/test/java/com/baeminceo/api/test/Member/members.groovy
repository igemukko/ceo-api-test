package com.baeminceo.api.test.Member

import com.baeminceo.api.test.utils.TestInit
import com.baeminceo.api.test.utils.UtilMethod
import io.restassured.RestAssured
import io.restassured.path.json.JsonPath

import static io.restassured.RestAssured.given


class members extends TestInit{

    def setup(){
        RestAssured.basePath = "/v1/members"
        sessionId = null
    }

    def "Member - [CEO] FIND 회원 by 검색조건" () {

        given:
        sessionId = UtilMethod.getSessionId("mooh02","a1111111")

        requestHeader.put("x-mem-no", "170213000004")
        requestHeader.put("x-mem-id", "mooh02")
//        requestHeader.put("Authorization",UtilMethod.allCookies)
        requestHeader.put("Authorization",UtilMethod.cookieValue)

        requestBody.put("memNo",null)
        requestBody.put("memId","mooh02")
        requestBody.put("memName",null)
        requestBody.put("email",null)
        requestBody.put("mobileNo",null)
        requestBody.put("leave",null)

        when:
        response = given().headers(requestHeader)
                    .log().all()
                    .formParams(requestBody)
                    .when().get()

        response.prettyPrint()
        jsonPath = new JsonPath(response.getBody().asString())

        then:
        response.getStatusCode()==200
    }

    def signOut(){
        UtilMethod.signOut(UtilMethod.cookieValue)
    }

}
