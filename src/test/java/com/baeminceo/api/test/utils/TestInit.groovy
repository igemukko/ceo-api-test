package com.baeminceo.api.test.utils

import io.restassured.RestAssured
import io.restassured.path.json.JsonPath
import io.restassured.response.Response
import spock.lang.Shared
import spock.lang.Specification

class TestInit extends Specification {

    @Shared Response response
    @Shared JsonPath jsonPath
    @Shared HashMap<String, String> requestHeader
    @Shared HashMap<String, String> requestDto
    @Shared String requestBody
    @Shared sessionId

    def setupSpec() {
        requestHeader = new HashMap<>()
        requestDto = new HashMap<>()

        RestAssured.baseURI = "http://dev-ceo.v2.test.com/"
    }

}
