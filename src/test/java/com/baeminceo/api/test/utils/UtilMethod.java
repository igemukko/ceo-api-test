package com.baeminceo.api.test.utils;


import com.github.fge.jsonschema.SchemaVersion;
import com.github.fge.jsonschema.cfg.ValidationConfiguration;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.spi.json.JacksonJsonNodeJsonProvider;
import com.jayway.jsonpath.spi.mapper.JacksonMappingProvider;
import io.restassured.http.Cookie;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.HashMap;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

public class UtilMethod {

    static String signInPath = "https://test.com/v1/signin";
    static String signOutPath = "https://test.com/v1/signout/{sessionId}";
    static HashMap<String, String> myCookie;
    static String resourcePath = System.getProperty("user.dir").concat("/src/test/resources/");

    public static String getEncodeAuthorizationInfo(String id, String pw){

        String basicAccessAuth = String.format("%s:%s", id, pw);
        String authorizationHeaderValue = "Basic " + Base64.getMimeEncoder(-1, new byte[]{}).encodeToString(basicAccessAuth.getBytes());

        System.out.println(authorizationHeaderValue);

        return authorizationHeaderValue;

    }

    public static Cookie getMyCookies(String id, String pw) {

        Response response;
        JsonPath jsonPath;

        response = given()
//                .log().all()
                .headers("Authorization", getEncodeAuthorizationInfo(id, pw))
                .post(signInPath);

//        response.prettyPrint();
        jsonPath = new JsonPath(response.getBody().asString());
        String sessionId = jsonPath.getJsonObject("sessionId");

        Cookie cookies = new Cookie.Builder("gk-session-id", sessionId).build();

//        Cookie cookie2 = new Cookie.Builder("connect.sid", "s%3AeSPbvhKupJRZD05RjPiB2Pg2Nga4E9iW.%2BRIAeRVMjg3UmRsXJ4WErmcl8h3UiJ5%2BcQFvTanhLsk").build();
//        Cookies cookies = new Cookies(cookie1, cookie2);

        return cookies;

    }

    public static Boolean signOut(String sessionId) {

        Response response;
        JsonPath jsonPath;
        Boolean result;

        response = given()
                .get(signOutPath, sessionId);

        jsonPath = new JsonPath(response.getBody().asString());
        result = jsonPath.get("success");
//        response.prettyPrint();

        return result;

    }

    //method of comapare json schema and json data
    public static void compareSchema(String schemaPath, Response res) {

         JsonSchemaFactory jsonSchemaFactory = JsonSchemaFactory.newBuilder()
                .setValidationConfiguration(ValidationConfiguration.newBuilder().setDefaultVersion(SchemaVersion.DRAFTV4).freeze())
                .freeze();

        res.then().assertThat().body(matchesJsonSchemaInClasspath(schemaPath).using(jsonSchemaFactory));
    }

    //return to json data
    public static String getJsonData(String path, JsonPath jp) {

        return jp.getJsonObject(path);
    }

    //json file read and translate string
    public static String generateStringFromResource(String filename) throws IOException {

        String filePath = resourcePath + filename;
        return new String(Files.readAllBytes(Paths.get(filePath)));

    }

    //update content of json file and save to file
    public static void updateToResource(String filename, String keyPath, String value) throws IOException {

        //file path
        String filePath = resourcePath + filename;
        //Read a file in string format
        String jsonObj = generateStringFromResource(filename);

        //Update json data
        Configuration configuration = Configuration.builder()
                .jsonProvider(new JacksonJsonNodeJsonProvider())
                .mappingProvider(new JacksonMappingProvider())
                .build();
        JsonNode updatedJson = com.jayway.jsonpath.JsonPath.using(configuration).parse(jsonObj).set("$."+keyPath, value).json();

        //Cast from JsonNode to JSONObject
        ObjectMapper mapper = new ObjectMapper();
        JSONObject convertJsonObj = mapper.convertValue(updatedJson, JSONObject.class);

        //Write file
        FileWriter file = new FileWriter(filePath, false);
        convertJsonObj.writeJSONString(convertJsonObj, file);
        file.close();
    }
}
