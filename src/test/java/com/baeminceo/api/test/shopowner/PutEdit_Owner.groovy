package com.baeminceo.api.test.shopowner

import com.baeminceo.api.test.utils.TestInit
import io.restassured.RestAssured
import io.restassured.path.json.JsonPath
import spock.lang.Unroll

import static com.baeminceo.api.test.utils.UtilMethod.generateStringFromResource
import static com.baeminceo.api.test.utils.UtilMethod.getMyCookies
import static io.restassured.RestAssured.given

class PutEdit_Owner extends TestInit{
    @Unroll
    def "#EXPLAIN 사장님사이트 일반업주 정보 수정"() {

        given:
        RestAssured.basePath = "/v1/shop_owners/edit/" + mem_id
        requestBody = generateStringFromResource(JSONPATH)

        when:
        response = given()
                .log().all()
                .header("Content-Type","application/json")
                .cookies(getMyCookies("mooh05", "a1111111"))
                .body(requestBody)
                .when().put()

        response.prettyPrint()
        jsonPath = new JsonPath(response.getBody().asString())

        then:
        println(EXPLAIN + "조회 결과:" + jsonPath.getJsonObject("statusCode")+"("+response.getStatusCode()+")")
        response.getStatusCode() == result


        where:
        mem_id      | JSONPATH                  | EXPLAIN           | result
        "mooh05"    | "PutEdit_Owner.json"      | "정상 아이디"        | 200
        "hisajang2" | "PutEdit_Owner.json"      | "다른 일반업주 아이디" | 403
        "mooh02"    | "PutEdit_Owner.json"      | "광고업주 아이디"     | 403
        "hisajang2‘ OR ‘text’ = N’text’"           | "PutEdit_Owner.json"      | "SQL Injection" | 404
        "hisajang2‘ OR ‘something’ like ‘some%’"   | "PutEdit_Owner.json"      | "SQL Injection" | 404
        "hisajang2‘OR2>1"                          | "PutEdit_Owner.json"      | "SQL Injection" | 404
        "hisajang2‘ OR ‘text’ > ‘t’"               | "PutEdit_Owner.json"      | "SQL Injection" | 404
        "hisajang2‘ OR ‘whatever’ in (‘whatever’)" | "PutEdit_Owner.json"      | "SQL Injection" | 404
    }
}
