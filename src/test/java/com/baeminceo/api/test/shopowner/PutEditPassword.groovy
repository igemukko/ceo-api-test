package com.baeminceo.api.test.shopowner

import com.baeminceo.api.test.utils.TestInit
import com.baeminceo.api.test.utils.UtilMethod
import io.restassured.RestAssured
import io.restassured.path.json.JsonPath
import spock.lang.Timeout
import spock.lang.Unroll

import java.util.concurrent.TimeUnit

import static com.baeminceo.api.test.utils.UtilMethod.compareSchema
import static com.baeminceo.api.test.utils.UtilMethod.getMyCookies
import static io.restassured.RestAssured.given



class PutEditPassword extends TestInit{

    @Unroll
    def "#EXPLAIN 사장님사이트 비밀번호 수정"() {
        given:
        RestAssured.basePath = "/v1/shop_owners/password/" + mem_id

        requestDto.put("currentPassword", cpw)
        requestDto.put("newPassword", npw )

        when:
        response = given()
                .log().all()
                .header("Content-Type","application/json")
                .cookies(getMyCookies("mooh02", "a1111111"))
                .body(requestDto)
                .when().put()

        response.prettyPrint()
        jsonPath = new JsonPath(response.getBody().asString())

        then:
        println(EXPLAIN + "수정 결과:" + jsonPath.getJsonObject("statusCode")+"("+response.getStatusCode()+")")
        response.getStatusCode() == result


        where:
        mem_id        | cpw               | npw         | EXPLAIN                      | result
        "bizbiz3"     | "qwer@1113"       | "qwer@1114" | "다른 멤버아이디 & 비번"          | 403
        "bizbiz3"     | "a1111111"        | "b1111111"  | "다른 멤버아이디 & 원래 멤버비번"    | 403
        null          | "a1111111"        | "b1111111"  | "멤버아이디 null"               | 403
        "mooh02"      | "11111111"        | "b1111111"  | "현재 비밀번호 잘못입력"           | 401
        "mooh02"      | "11111111‘ having 1=1--"  | "b1111111"  | "SQL Injection - cpw"        | 401
        "mooh02"      | "a1111111"        | "11111111‘ group by userid having 1=1--"    | "SQL Injection - npw" | 400
        "bizbiz3%22+or+isnull%281%2F0%29+%2F*"   | "a1111111"  | "b1111111" | "SQL Injection" | 403
        "bizbiz3%27+--+"     | "a1111111"        | "b1111111"  | "SQL Injection"              | 403
        "bizbiz3‘ or 1=1--"  | "a1111111"        | "b1111111"  | "SQL Injection"              | 403
        "“ or 1=1--"         | "a1111111"        | "b1111111"  | "SQL Injection"              | 403
        "bizbiz3‘or1=1/*"    | "a1111111"        | "b1111111"  | "SQL Injection"              | 403
    }
}
