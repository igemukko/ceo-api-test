package com.baeminceo.api.test.shopowner

import com.baeminceo.api.test.utils.TestInit
import com.baeminceo.api.test.utils.UtilMethod
import io.restassured.RestAssured
import io.restassured.path.json.JsonPath
import spock.lang.Timeout
import spock.lang.Unroll

import java.util.concurrent.TimeUnit

import static com.baeminceo.api.test.utils.UtilMethod.compareSchema
import static com.baeminceo.api.test.utils.UtilMethod.generateStringFromResource
import static com.baeminceo.api.test.utils.UtilMethod.getMyCookies
import static com.baeminceo.api.test.utils.UtilMethod.updateToResource
import static io.restassured.RestAssured.given


class PutEditShopManager_Password extends TestInit {

    @Unroll
    def "#EXPLAIN 사장님사이트 운영자정보&Password 수정"() {

        given:
        RestAssured.basePath = "/v1/shop_owners/edit_shop_manager_and_password/" + mem_id
        requestBody = generateStringFromResource(JSONPATH)

        when:
        response = given()
                .log().all()
                .header("Content-Type","application/json")
                .cookies(getMyCookies("mooh02", "a1111111"))
                .body(requestBody)
                .when().put()

        response.prettyPrint()
        jsonPath = new JsonPath(response.getBody().asString())

        then:
        println(EXPLAIN + "조회 결과:" + jsonPath.getJsonObject("statusCode")+"("+response.getStatusCode()+")")
        response.getStatusCode() == result


        where:
        mem_id    | JSONPATH                        | EXPLAIN           | result
        "mooh02"  | "puteditshopmanager_Password.json" | "정상 아이디"        | 200
        "bizbiz3" | "puteditshopmanager_Password.json" | "다른 멤버아이디"     | 403
        " "       | "puteditshopmanager_Password.json" | "멤버아이디 null"    | 404
        "bizbiz3) UNION SELECT%20*%20FROM%20INFORMATION_SCHEMA. TABLES;" | "puteditshopmanager_memid.json" | "SQL Injection" | 404
        "bizbiz3‘ group by userid having 1=1--"                          | "puteditshopmanager_memid.json" | "SQL Injection" | 404
        "bizbiz3‘ SELECT name FROM syscolumns WHERE id = (SELECT id FROM sysobjects WHERE name = tablename’)--"| "puteditshopmanager_memid.json" | "SQL Injection" | 404
        "bizbiz3‘ union all select @@version--"                          | "puteditshopmanager_memid.json" | "SQL Injection" | 404
        "bizbiz3‘ or 1 in (select @@version)--"                          | "puteditshopmanager_memid.json" | "SQL Injection" | 404
    }
}