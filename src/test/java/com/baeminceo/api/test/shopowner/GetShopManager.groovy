package com.baeminceo.api.test.shopowner

import com.baeminceo.api.test.utils.TestInit
import com.baeminceo.api.test.utils.UtilMethod
import io.restassured.RestAssured
import io.restassured.path.json.JsonPath
import spock.lang.Timeout
import spock.lang.Unroll

import java.util.concurrent.TimeUnit

import static com.baeminceo.api.test.utils.UtilMethod.compareSchema
import static com.baeminceo.api.test.utils.UtilMethod.getMyCookies
import static io.restassured.RestAssured.given


class GetShopManager extends TestInit {
    @Unroll
    def "#EXPLAIN로 운영자 정보 가져오기"() {
        given:
        RestAssured.basePath = "/v1/shop_owners/" + shopowner_no + "/shop_manager"

        when:
        response = given()
                .log().all()
                .cookies(getMyCookies("mooh02", "a1111111"))
                .when().get()

        response.prettyPrint()
        jsonPath = new JsonPath(response.getBody().asString())

        then:
        response.getStatusCode() == result
        println(EXPLAIN + "조회 결과:" + jsonPath.getJsonObject("statusCode") + "(" + response.getStatusCode() + ")")

        where:
        shopowner_no   | EXPLAIN             | result
        "201702130001" | "정상 업주번호"        | 200
        "111111111111111111" | "잘못된 업주번호" | 403
        "201709140008" | "다른 업주의 업주번호"   | 403
        null           | "잘못된 업주번호 null"  | 404
        "201709140008 OR 1=1"       | "SQL Injection"     | 403
        "201709140008‘ OR ‘1’=’1"  | "SQL Injection"     | 403
        "201709140008; OR ‘1’=’1’" | "SQL Injection"     | 403
        "201709140008%22+or+isnull%281%2F0%29+%2F*" | "SQL Injection"     | 403
        "201709140008%27+OR+%277659%27%3D%277659"   | "SQL Injection"     | 403
    }
}
