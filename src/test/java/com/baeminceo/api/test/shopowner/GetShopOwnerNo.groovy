package com.baeminceo.api.test.shopowner

import com.baeminceo.api.test.utils.TestInit
import io.restassured.RestAssured
import io.restassured.path.json.JsonPath
import spock.lang.Unroll

import static com.baeminceo.api.test.utils.UtilMethod.getMyCookies
import static io.restassured.RestAssured.given


class GetShopOwnerNo extends TestInit{

    @Unroll
    def "#EXPLAIN로 업주정보 조회"() {

        given:
        RestAssured.basePath = "/v1/shop_owners/" + shopowner_no

        when:
        response = given()
                .log().all()
                .cookies(getMyCookies("mooh02", "a1111111"))
                .when().get()

        response.prettyPrint()
        jsonPath = new JsonPath(response.getBody().asString())

        then:
        println(EXPLAIN + "조회 결과:" + jsonPath.getJsonObject("statusCode")+"("+response.getStatusCode()+")")
        response.getStatusCode() == result

        where:
        shopowner_no                    | EXPLAIN             |result
        "201702130001"                  | "정상 업주번호"        | 200
        "111111111aaa11"                | "잘못된 업주번호"       | 403
        "201709140008"                  | "다른 업주의 업주번호"   | 403
        null                            | "잘못된 업주번호 null"  | 404
        "201709140008‘||(elt(-3+5,bin(15),ord(10),hex(char(45))))" | "SQL Injection"     | 404
        "201709140008||6"                           | "SQL Injection"     | 403
        "201709140008‘||’6"                         | "SQL Injection"     | 403
        "201709140008(||6)"                         | "SQL Injection"     | 403
        "201709140008‘ OR 1=1--"                    | "SQL Injection"     | 403



    }
}