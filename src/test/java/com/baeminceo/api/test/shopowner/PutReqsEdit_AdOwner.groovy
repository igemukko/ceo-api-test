package com.baeminceo.api.test.shopowner

import com.baeminceo.api.test.utils.TestInit
import io.restassured.RestAssured
import io.restassured.path.json.JsonPath
import spock.lang.Unroll

import static com.baeminceo.api.test.utils.UtilMethod.generateStringFromResource
import static com.baeminceo.api.test.utils.UtilMethod.getMyCookies
import static io.restassured.RestAssured.given


class PutReqsEdit_AdOwner extends TestInit {
    @Unroll
    def "#EXPLAIN 사장님사이트 광고업주 정보 수정"() {

        given:
        RestAssured.basePath = "/v1/shop_owners_reqs/edit/" + mem_id
        requestBody = generateStringFromResource(JSONPATH)

        when:
        response = given()
                .log().all()
                .header("Content-Type","application/json")
                .cookies(getMyCookies("bizbiz2", "qwer@1234"))
                .body(requestBody)
                .when().put()

        response.prettyPrint()
        jsonPath = new JsonPath(response.getBody().asString())

        then:
        println(EXPLAIN + "결과:" + jsonPath.getJsonObject("statusCode")+"("+response.getStatusCode()+")")
        response.getStatusCode() == result


        where:
        mem_id    | JSONPATH                        | EXPLAIN           | result
        "bizbiz2" | "PutReqsEdit_AdOwner.json"      | "정상 아이디"        | 200
        "bizibiz1"| "PutReqsEdit_AdOwner.json"      | "다른 광고업주 아이디" | 403
        "bizbiz3" | "PutReqsEdit_AdOwner.json"      | "수정요청중인 아이디"  | 403
        "mooh05"  | "PutReqsEdit_AdOwner.json"      | "일반업주 아이디"     | 403
        "bizibiz1‘ OR ‘text’ = N’text’"           | "PutReqsEdit_AdOwner.json"      | "SQL Injection" | 403
        "bizibiz1‘ OR ‘something’ like ‘some%’"   | "PutReqsEdit_AdOwner.json"      | "SQL Injection" | 403
        "bizibiz1‘OR2>1"                          | "PutReqsEdit_AdOwner.json"      | "SQL Injection" | 403
        "bizibiz1‘ OR ‘text’ > ‘t’"               | "PutReqsEdit_AdOwner.json"      | "SQL Injection" | 403
        "bizibiz1‘ OR ‘whatever’ in (‘whatever’)" | "PutReqsEdit_AdOwner.json"      | "SQL Injection" | 403
    }
}
