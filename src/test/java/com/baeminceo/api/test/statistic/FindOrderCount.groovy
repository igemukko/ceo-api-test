package com.baeminceo.api.test.statistic

import com.baeminceo.api.test.utils.TestInit
import io.restassured.RestAssured
import io.restassured.path.json.JsonPath
import spock.lang.Unroll

import static com.baeminceo.api.test.utils.UtilMethod.getJsonData
import static com.baeminceo.api.test.utils.UtilMethod.getMyCookies
import static io.restassured.RestAssured.given

class FindOrderCount extends TestInit {

    def setup() {
        RestAssured.basePath = "/v1/statistic/order_count"
    }

    /*
    * 소유 업소의 일별, 주별, 월별 주문수를 조회한다.
    * 원 api: 헤더에 x-mem-no 및 x-shopowner-no를 포함해서 api 요청
    * 수정 api: 업주/업소 정보가 변조되어 파라미터로 전송되었을 경우 front 에서 403 에러
    * 본 api는 파라미터로 전송하는 정보는 업소번호임
    * 소유 업소 여부에 대한 유효성 체크하여 front 에서 403 에러 처리
    */
    @Unroll
    def "[CEO] FIND 주문수 통계 - #EXPLAIN"() {

        given: "검색 조건 설정"
        requestDto.put("from", FROM)
        requestDto.put("to", TO)
        requestDto.put("shopNo", SHOPNO)
        requestDto.put("interval", INTERVAL)

        when:
        response = given()
                .log().all()
                .cookies(getMyCookies(LOGINID, LOGINPW))
                .params(requestDto)
                .get()

        response.prettyPrint()
        jsonPath = new JsonPath(response.getBody().asString())

        then:
        response.getStatusCode() == EXPECTRE1
        getJsonData("statusCode", jsonPath).contains(EXPECTRE2) == true

        where:
        LOGINID     |   LOGINPW     |   FROM            |   TO              |   SHOPNO                                  |   INTERVAL    ||  EXPECTRE1   |   EXPECTRE2   |   EXPLAIN
        "lee00m"    |   "111111"    |   "2016-10-01"    |   "2016-10-31"    |   null                                    |   "w"         ||  200         |   "SUCCESS"   |   "모든 소유 업소 정보 조회"
        "lee00m"    |   "111111"    |   "2016-10-01"    |   "2016-10-31"    |   "445078"                                |   "w"         ||  200         |   "SUCCESS"   |   "소유 업소 정보 조회"
        "lee00m"    |   "111111"    |   "2016-10-01"    |   "2016-10-31"    |   "658428"                                |   "w"         ||  403         |   "forbidden" |   "미소유 업소 정보 조회"
        "lee00m"    |   "111111"    |   "2016-10-01"    |   "2016-10-31"    |   "abcdefghijklmnopqrstuvwxyz1234567890"  |   "w"         ||  403         |   "forbidden" |   "유효하지 않은 업소 조회(문자+숫자)"
        'lee00m'    |   "111111"    |   "2016-10-01"    |   "2016-10-31"    |   "000000"                                |   'w'         ||  403         |   "forbidden" |   "없는 업소 조회(000000)"
        "lee00m"    |   "111111"    |   "2016-10-01"    |   "2016-10-31"    |   "123456789012345678901234567890123456"  |   "w"         ||  403         |   "forbidden" |   "유효하지 않은 업소(긴 숫자)"
        "lee00m"    |   "111111"    |   "2016-10-01"    |   "2016-10-31"                                                          |   "658428‘ union all select @@version-- "        |   "w"                                        ||  403         |   "forbidden" |   "sql injection"
        "lee00m"    |   "111111"    |   "2016-10-01"    |   "2016-10-31"                                                          |   "658428‘ OR ‘unusual’ = ‘unusual’"            |   "w"                                        ||  403         |   "forbidden" |   "sql injection"
        "lee00m"    |   "111111"    |   "2016-10-01"    |   "2016-10-31"                                                          |   "658428‘ OR ‘something’ = ‘some’+’thing’"     |   "w"                                        ||  403         |   "forbidden" |   "sql injection"
        "lee00m"    |   "111111"    |   "2016-10-01"    |   "2016-10-31"                                                          |   "658428‘ OR ‘text’ = N’text’"                 |   "w"                                        ||  403         |   "forbidden" |   "sql injection"
        "lee00m"    |   "111111"    |   "2016-10-01"    |   "2016-10-31"                                                          |   "658428‘ OR ‘something’ like ‘some%’"         |   "w"                                        ||  403         |   "forbidden" |   "sql injection"
        "lee00m"    |   "111111"    |   "2016-10-01"    |   "2016-10-31"                                                          |   "445078"                                |   "w‘ OR 2 > 1 "                               ||  400         |   "Invalid"   |   "sql injection"
        "lee00m"    |   "111111"    |   "2016-10-01"    |   "2016-10-31"                                                          |   "445078"                                |   "w‘ OR ‘text’ > ‘t’ "                        ||  400         |   "Invalid"   |   "sql injection"
        "lee00m"    |   "111111"    |   "2016-10-01"    |   "2016-10-31"                                                          |   "445078"                                |   "w‘ OR ‘whatever’ in (‘whatever’)"          ||  400         |   "Invalid"   |   "sql injection"
        "lee00m"    |   "111111"    |   "2016-10-01"    |   "2016-10-31"                                                          |   "445078"                                |   "w‘ OR 2 BETWEEN 1 and 3 "                   ||  400         |   "Invalid"   |   "sql injection"
        "lee00m"    |   "111111"    |   "2016-10-01"    |   "2016-10-31"                                                          |   "445078"                                |   "w‘ or username like char(37);"             ||  400         |   "Invalid"   |   "sql injection"
        "lee00m"    |   "111111"    |   "2016-10-01"    |   "2016-10-31‘ union select * from users where login = char(114,111,111,116); "    |   "445078"                                |   "w"                                        ||  400         |   "Invalid"   |   "sql injection"
        "lee00m"    |   "111111"    |   "2016-10-01"    |   "2016-10-31‘ union select"                                                      |   "445078"                                |   "w"                                        ||  400         |   "Invalid"   |   "sql injection"
        "lee00m"    |   "111111"    |   "2016-10-01"    |   "2016-10-31 Password:*/=1--"                                                     |   "445078"                                |   "w"                                        ||  400         |   "Invalid"   |   "sql injection"
        "lee00m"    |   "111111"    |   "2016-10-01"    |   "2016-10-31 UNI/**/ON SEL/**/ECT"                                                |   "445078"                                |   "w"                                        ||  400         |   "Invalid"   |   "sql injection"
        "lee00m"    |   "111111"    |   "2016-10-01"    |   "2016-10-31‘; EXECUTE IMMEDIATE ‘SEL’ || ‘ECT US’ || ‘ER’"                      |   "445078"                                |   "w"                                        ||  400         |   "Invalid"   |   "sql injection"

    }

}
