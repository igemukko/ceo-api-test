package com.baeminceo.api.test.statistic

import com.baeminceo.api.test.utils.TestInit
import io.restassured.RestAssured
import io.restassured.path.json.JsonPath
import io.restassured.response.Response
import spock.lang.Unroll

import static com.baeminceo.api.test.utils.UtilMethod.getJsonData
import static com.baeminceo.api.test.utils.UtilMethod.getMyCookies
import static io.restassured.RestAssured.given

class FindCallCount extends TestInit {

    def setup() {
        RestAssured.basePath = "/v1/statistic/call_count"
    }

    /*
    * 소유 업소의 일별, 주별 월별 통화수를 조회한다.
    * 원 api: 헤더에 x-mem-no 및 x-shopowner-no를 포함해서 api 요청
    * 수정 api: 업주/업소 정보가 변조되어 파라미터로 전송되었을 경우 front 에서 403 에러
    * 본 api는 url 파라미터로 전송하는 정보는 업소번호임
    * 소유 업소 여부에 대한 유효성 체크하여 front 에서 403 에러 처리
    */
    @Unroll
    def "[CEO] FIND 통화수 - #EXPLAIN"() {

        given: "검색 조건 설정"
        requestDto.put("from", FROM)
        requestDto.put("to", TO)
        requestDto.put("shopNo", SHOPNO)
        requestDto.put("interval", INTERVAL)

        when:
        response = given()
                .log().all()
                .cookies(getMyCookies(LOGINID, LOGINPW))
                .params(requestDto)
                .get()

        response.prettyPrint()
        jsonPath = new JsonPath(response.getBody().asString())

        then:
        response.getStatusCode() == EXPECTRE1
        getJsonData("statusCode", jsonPath).contains(EXPECTRE2) == true

        where:
        LOGINID     |   LOGINPW     |   FROM            |   TO                                                      |   SHOPNO                                   |   INTERVAL                                       ||  EXPECTRE1   |   EXPECTRE2   |   EXPLAIN
        "lee00m"    |   "111111"    |   "2016-10-01"    |   "2016-10-31"                                            |   null                                     |   "w"                                            ||  200         |   "SUCCESS"   |   "모든 소유 업소"
        "lee00m"    |   "111111"    |   "2016-10-01"    |   "2016-10-31"                                            |   "445078"                                 |   "w"                                            ||  200         |   "SUCCESS"   |   "소유 업소"
        "lee00m"    |   "111111"    |   "2016-10-01"    |   "2016-10-31"                                            |   "658428"                                 |   "w"                                            ||  403         |   "forbidden" |   "미소유 업소"
        "lee00m"    |   "111111"    |   "2016-10-01"    |   "2016-10-31"                                            |   "abcdefghijklmnopqrstuvwxyz1234567890"   |   "w"                                            ||  403         |   "forbidden" |   "유효하지 않은 업소(문자+숫자)"
        'lee00m'    |   "111111"    |   "2016-10-01"    |   "2016-10-31"                                            |   "000000"                                 |   'w'                                            ||  403         |   "forbidden" |   "없는 업소(000000)"
        "lee00m"    |   "111111"    |   "2016-10-01"    |   "2016-10-31"                                                          |   "658428 OR 1=1  "                             |   "w"                                        ||  403         |   "forbidden" |   "sql injection"
        "lee00m"    |   "111111"    |   "2016-10-01"    |   "2016-10-31"                                                          |   "658428‘ OR ‘1’=’1 "                         |   "w"                                        ||  403         |   "forbidden" |   "sql injection"
        "lee00m"    |   "111111"    |   "2016-10-01"    |   "2016-10-31"                                                          |   "658428; OR ‘1’=’1’"                        |   "w"                                        ||  403         |   "forbidden" |   "sql injection"
        "lee00m"    |   "111111"    |   "2016-10-01"    |   "2016-10-31"                                                          |   "658428%22+or+isnull%281%2F0%29+%2F*"       |   "w"                                        ||  403         |   "forbidden" |   "sql injection"
        "lee00m"    |   "111111"    |   "2016-10-01"    |   "2016-10-31"                                                          |   "658428%27+OR+%277659%27%3D%277659"         |   "w"                                        ||  403         |   "forbidden" |   "sql injection"
        "lee00m"    |   "111111"    |   "2016-10-01"    |   "2016-10-31"                                                          |   "445078"                                    |   "w%22+or+isnull%281%2F0%29+%2F* "           ||  400         |   "Invalid"   |   "sql injection"
        "lee00m"    |   "111111"    |   "2016-10-01"    |   "2016-10-31"                                                          |   "445078"                                    |   "w%27+--+ "                                 ||  400         |   "Invalid"   |   "sql injection"
        "lee00m"    |   "111111"    |   "2016-10-01"    |   "2016-10-31"                                                          |   "445078"                                    |   "w‘ or 1=1--"                              ||  400         |   "Invalid"   |   "sql injection"
        "lee00m"    |   "111111"    |   "2016-10-01"    |   "2016-10-31"                                                          |   "445078"                                    |   "w“ or 1=1-- "                              ||  400         |   "Invalid"   |   "sql injection"
        "lee00m"    |   "111111"    |   "2016-10-01"    |   "2016-10-31"                                                          |   "445078"                                    |   "w‘or1=1/*"                                ||  400         |   "Invalid"   |   "sql injection"
        "lee00m"    |   "111111"    |   "2016-10-01"    |   "2016-10-01 or 1=1--"                                                 |   "445078"                                    |   "w"                                        ||  400         |   "Invalid"   |   "sql injection"
        "lee00m"    |   "111111"    |   "2016-10-01"    |   "2016-10-01‘ or ‘a’=’a"                                               |   "445078"                                    |   "w"                                        ||  400         |   "Invalid"   |   "sql injection"
        "lee00m"    |   "111111"    |   "2016-10-01"    |   "2016-10-01“ or “a”=”a"                                               |   "445078"                                    |   "w"                                        ||  400         |   "Invalid"   |   "sql injection"
        "lee00m"    |   "111111"    |   "2016-10-01"    |   "2016-10-01‘) or (‘a’=’a "                                             |   "445078"                                    |   "w"                                        ||  400         |   "Invalid"   |   "sql injection"
        "lee00m"    |   "111111"    |   "2016-10-01"    |   "Admin’ OR ‘ ‘%20SELECT%20*%20FROM%20INFORMATION_SCHEMA. TABLES--"    |   "445078"                                    |   "w"                                        ||  400         |   "Invalid"   |   "sql injection"
    }
}
