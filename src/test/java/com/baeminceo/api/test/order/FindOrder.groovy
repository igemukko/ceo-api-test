package com.baeminceo.api.test.order

import com.baeminceo.api.test.utils.TestInit
import io.restassured.RestAssured
import io.restassured.http.ContentType
import io.restassured.path.json.JsonPath
import spock.lang.Unroll

import static com.baeminceo.api.test.utils.UtilMethod.getJsonData
import static com.baeminceo.api.test.utils.UtilMethod.getJsonData
import static com.baeminceo.api.test.utils.UtilMethod.getMyCookies
import static io.restassured.RestAssured.given

class FindOrder extends TestInit {

    def setup() {
        RestAssured.basePath = "/v1/orders"
    }

    /*
    * 사용자 업소의 바로결제 주문내역을 제공합니다.
    * 원 api: 헤더에 x-shopowner-no 및 x-mem-no를 포함해서 api 요청
    * 수정 api:
    * 본 api는 쿼리 파라미터로 전송하는 정보는 조회 시작일 및 종료정보, 주문 상태 정보임
    * 로그인한 업주의 주문 상태에 따른 모든 주문 내역을 정상적으로 가져와야 함
    */
    @Unroll
    def "[CEO] FIND 바로결제 주문내역 - #EXPLAIN" () {

        given: "헤더 정보 설정"
        requestHeader.put("Content-Type", ContentType.JSON)

        requestDto.put("from", FROM)
        requestDto.put("to", TO)
        requestDto.put("ordProgCd", 2)



        when:
        response = given()
                .log().all()
                .cookies(getMyCookies(LOGINID, LOGINPW))
                .headers(requestHeader)
                .queryParams(requestDto)
                .get()

        response.prettyPrint()
        jsonPath = new JsonPath(response.getBody().asString())

        then:
        response.getStatusCode() == EXPECTRE1
        getJsonData("statusCode", jsonPath) == null ? EXPECTRE2 : getJsonData("statusCode", jsonPath).contains(EXPECTRE2) == true


        where:
        LOGINID     |   LOGINPW     |   FROM           |   TO              |    ordProgCd   ||  EXPECTRE1   |   EXPECTRE2   |   EXPLAIN
        "kimhj5"    |   "kim54321"  |   "2017-08-05"   |   "2017-08-05"    |    2           ||  200         |   "SUCCESS"   |   "정상 사장님 및 주문번호"
        "kimhj5"    |   "kim54321"  |   "2017-08-05||6"          |   "2017-08-05"    |    2           ||  400         |   "InvalidParameter"   |   "SQL Injection - from"
        "kimhj5"    |   "kim54321"  |   "2017-08-05‘||’6"        |   "2017-08-05"    |    2           ||  400         |   "InvalidParameter"   |   "SQL Injection - from"
        "kimhj5"    |   "kim54321"  |   "2017-08-05(||6)"        |   "2017-08-05"    |    2           ||  400         |   "InvalidParameter"   |   "SQL Injection - from"
        "kimhj5"    |   "kim54321"  |   "2017-08-05‘ OR 1=1--"   |   "2017-08-05"    |    2           ||  400         |   "InvalidParameter"   |   "SQL Injection - from"
        "kimhj5"    |   "kim54321"  |   "2017-08-05‘||(elt(-3+5,bin(15),ord(10),hex(char(45))))"   |   "2017-08-05"    |    2           ||  400         |   "InvalidParameter"   |   "SQL Injection - from"
        "kimhj5"    |   "kim54321"  |   "2017-08-05"   |   "2017-08-05‘ group by userid having 1=1--" |    2           ||  400         |   "InvalidParameter"   |   "SQL Injection - to"
        "kimhj5"    |   "kim54321"  |   "2017-08-05"   |   "2017-08-05‘ SELECT name FROM syscolumns WHERE id = (SELECT id FROM sysobjects WHERE name = tablename’)--"    |    2           ||  400         |   "InvalidParameter"   |   "SQL Injection - to"
        "kimhj5"    |   "kim54321"  |   "2017-08-05"   |   "2017-08-05‘ or 1 in (select @@version)--" |    2           ||  400         |   "InvalidParameter"   |   "SQL Injection - to"
        "kimhj5"    |   "kim54321"  |   "2017-08-05"   |   "2017-08-05‘ union all select @@version--" |    2           ||  400         |   "InvalidParameter"   |   "SQL Injection - to"
        "kimhj5"    |   "kim54321"  |   "2017-08-05"   |   "2017-08-05‘ OR ‘unusual’ = ‘unusual’"     |    2           ||  400         |   "InvalidParameter"   |   "SQL Injection - to"
        "kimhj5"    |   "kim54321"  |   "2017-08-05"   |   "2017-08-05"    | "2 ‘ OR 1=1--" ||  400     |   "InvalidParameter"   |   "SQL Injection - ordProgCd"
        "kimhj5"    |   "kim54321"  |   "2017-08-05"   |   "2017-08-05"    | "2‘ OR ‘text’ = N’text’"             ||  400     |   "InvalidParameter"   |   "SQL Injection - ordProgCd"
        "kimhj5"    |   "kim54321"  |   "2017-08-05"   |   "2017-08-05"    | "2‘ OR ‘something’ like ‘some%’"     ||  400     |   "InvalidParameter"   |   "SQL Injection - ordProgCd"
        "kimhj5"    |   "kim54321"  |   "2017-08-05"   |   "2017-08-05"    | "2‘ OR ‘whatever’ in (‘whatever’)"   ||  400     |   "InvalidParameter"   |   "SQL Injection - ordProgCd"
        "kimhj5"    |   "kim54321"  |   "2017-08-05"   |   "2017-08-05"    | "2‘OR2BETWEEN1and3"                  ||  400     |   "InvalidParameter"   |   "SQL Injection - ordProgCd"
    }
}