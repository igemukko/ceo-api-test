package com.baeminceo.api.test.order

import com.baeminceo.api.test.utils.TestInit
import io.restassured.RestAssured
import io.restassured.http.ContentType
import io.restassured.path.json.JsonPath
import spock.lang.Unroll

import static com.baeminceo.api.test.utils.UtilMethod.getJsonData
import static com.baeminceo.api.test.utils.UtilMethod.getJsonData
import static com.baeminceo.api.test.utils.UtilMethod.getMyCookies
import static io.restassured.RestAssured.given

class FindOrderDetail extends TestInit {

    def setup() {
        RestAssured.basePath = "/v1/orders/{orderNo}"
    }

    /*
    * 사용자 업소의 바로결제 주문 상세내역을 제공합니다.
    * 원 api: 헤더에 x-shopowner-no 및 x-mem-no를 포함해서 api 요청
    * 수정 api:
    * 본 api는 url 파라미터로 전송하는 정보는 주문번호와 주문일 정보
    * 주문번호에 대한 유효성은 B2B 서버에서 체크해야
    */
    @Unroll
    def "[CEO] FIND 바로결제 주문상세내역 - #EXPLAIN" () {

        given: "헤더 정보 설정"
        requestHeader.put("Content-Type", ContentType.JSON)
//        requestHeader.put("memNo",MEMNO)

        requestDto.put("orderDt", ORDERDATE)
//        requestDto.put("shopOwnerNo", SHOPOWNERNO)



        when:
        response = given()
                .log().all()
                .cookies(getMyCookies(LOGINID, LOGINPW))
                .headers(requestHeader)
                .pathParam("orderNo", ORDERNO)
                .queryParams(requestDto)
                .get()

        response.prettyPrint()
        jsonPath = new JsonPath(response.getBody().asString())

        then:
        response.getStatusCode() == EXPECTRE1
        getJsonData("statusCode", jsonPath) == null ? EXPECTRE2 : getJsonData("statusCode", jsonPath).contains(EXPECTRE2) == true


        where:
        LOGINID     |   LOGINPW     |   ORDERNO                         |   ORDERDATE       ||  EXPECTRE1   |   EXPECTRE2   |   EXPLAIN
        "kimhj5"    |   "kim54321"  |   "BJLNG00298"                    |   "2017-08-05"    ||  200         |   "SUCCESS"   |   "정상 사장님 및 주문번호"
        "kimhj5"    |   "kim54321"  |   "BLMUR00016"                    |   "2017-08-05"    ||  400         |   "Invalid"   |   "다른 업소의 주문정보"
        "kimhj5"    |   "kim54321"  |   " "                             |   "2017-08-05"    ||  400         |   "Invalid"   |   "유효하지 않은 주문번호(공백)"
        "kimhj5"    |   "kim54321"  |   "BJLNG'''''''''''''''''''00298" |   "2017-08-05"    ||  400         |   "Invalid"   |   "유효하지 않은 주문번호(긴자릿수)"
        "kimhj5"    |   "kim54321"  |   "BJLNG00000"                    |   "2017-08-05"    ||  400         |   "Invalid"   |   "없는 주문번호"
        "kimhj5"    |   "kim54321"  |   "BJLNG00000‘; EXECUTE IMMEDIATE ‘SEL’ || ‘ECT US’ || ‘ER’" |   "2017-08-05"    ||  400         |   "Invalid"   |   "SQL Injection"
        "kimhj5"    |   "kim54321"  |   "BJLNG00000‘ union select * from users where login = char(114,111,111,116);" |   "2017-08-05"    ||  400         |   "Invalid"   |   "SQL Injection"
        "kimhj5"    |   "kim54321"  |   "BJLNG00000‘ and 1 in (select var from temp)--"            |   "2017-08-05"    ||  400         |   "Invalid"   |   "SQL Injection"
        "kimhj5"    |   "kim54321"  |   "BJLNG00000 UNI/**/ON SEL/**/ECT"                          |   "2017-08-05"    ||  404         |   null        |   "SQL Injection"
        "kimhj5"    |   "kim54321"  |   "BJLNG00000‘ OR ‘something’ = ‘some’+’thing’"              |   "2017-08-05"    ||  400         |   "Invalid"   |   "SQL Injection"
        "kimhj5"    |   "kim54321"  |   "BLMUR00016"                    |   "BJLNG00000‘ OR ‘text’ = N’text’"          ||  400         |   "Invalid"   |   "SQL Injection"
        "kimhj5"    |   "kim54321"  |   "BLMUR00016"                    |   "BJLNG00000‘ or username like char(37);"   ||  400         |   "Invalid"   |   "SQL Injection"
        "kimhj5"    |   "kim54321"  |   "BLMUR00016"                    |   "BJLNG00000‘ and 1 in (select var from temp)--" ||  400         |   "Invalid"   |   "SQL Injection"
        "kimhj5"    |   "kim54321"  |   "BLMUR00016"                    |   "BJLNG00000 UNI/**/ON SEL/**/ECT"           ||  400         |   "Invalid"   |   "SQL Injection"
        "kimhj5"    |   "kim54321"  |   "BLMUR00016"                    |   "BJLNG00000‘or1/*"                          ||  400         |   "Invalid"   |   "SQL Injection"


    }
}
