package com.baeminceo.api.test.signup

import com.baeminceo.api.test.utils.TestInit
import io.restassured.RestAssured
import io.restassured.path.json.JsonPath
import spock.lang.Unroll

import static com.baeminceo.api.test.utils.UtilMethod.getMyCookies
import static io.restassured.RestAssured.given


class GetImrShopOwnerStatus extends TestInit{
    @Unroll
    def "#EXPLAIN로 정보수정요청 진행상태 조회"() {
        given:
        RestAssured.basePath = "/v1/signup/imr_shop_owner_status/" + shopowner_no

        when:
        response = given()
                .log().all()
                .cookies(getMyCookies("decotest201907010279", "qwer1234"))
                .when().get()

        response.prettyPrint()
        jsonPath = new JsonPath(response.getBody().asString())

        then:
        println(EXPLAIN + "조회 결과:" + jsonPath.getJsonObject("statusCode") + "(" + response.getStatusCode() + ")")
        response.getStatusCode() == result

        where:
        shopowner_no   | EXPLAIN             | result
        "201702130001" | "정상 업주번호"        | 200
        "111111111111" | "잘못된 업주번호"       | 404
        "201709140008" | "다른 업주의 업주번호"   | 403
        null           | "잘못된 업주번호 null"  | 404
        "201709140008‘ SELECT name FROM syscolumns WHERE id = (SELECT id FROM sysobjects WHERE name = tablename’)--" | "SQL Injection"  | 404
        "201709140008‘ or 1 in (select @@version)--"     | "SQL Injection"  | 404
        "201709140008‘ union all select @@version--"     | "SQL Injection"  | 404
        "201709140008‘ OR ‘unusual’ = ‘unusual’"         | "SQL Injection"  | 404
        "201709140008‘ OR ‘something’ = ‘some’+’thing’"  | "SQL Injection"  | 404
    }
}
