package com.baeminceo.api.test.log

import com.baeminceo.api.test.utils.TestInit
import io.restassured.RestAssured
import io.restassured.path.json.JsonPath
import spock.lang.Unroll

import static com.baeminceo.api.test.utils.UtilMethod.getMyCookies
import static io.restassured.RestAssured.given


class PutSuperlogClear_Soldout extends TestInit{
    @Unroll
    def "#EXPLAIN 재료소진 해제 로그 추가"() {

        given:
        RestAssured.basePath = "/v1/superlog/clear_soldout"

        requestDto.put("itemGroupName", "치킨")
        requestDto.put("itemName", "재료소진 테스트용")
        requestDto.put("memId",mem_id)
        requestDto.put("shopNo",shop_no)

        when:
        response = given()
                .log().all()
                .header("Content-Type","application/json")
                .cookies(getMyCookies("mooh02", "a1111111"))
                .body(requestDto)
                .when().put()

        response.prettyPrint()
        jsonPath = new JsonPath(response.getBody().asString())

        then:
        println(EXPLAIN + "조회 결과:" + jsonPath.getJsonObject("statusCode")+"("+response.getStatusCode()+")")
        response.getStatusCode() == result


        where:
        mem_id    | shop_no  |  EXPLAIN           | result
        "mooh02"  | "658428" | "정상 아이디"         | 200
        "bizbiz3" | "658428" | "다른 멤버아이디"      | 403
        "mooh02"  | "445078" | "다른 업주의 업소"     | 400
        "bizbiz3‘or1/*"                        | "658428" | "SQL Injection" | 404
        "bizbiz3+or+isnull%281%2F0%29+%2F*"    | "658428" | "SQL Injection" | 404
        "bizbiz3%27+OR+%277659%27%3D%277659"   | "658428" | "SQL Injection" | 404
        "bizbiz3%22+or+isnull%281%2F0%29+%2F*" | "658428" | "SQL Injection" | 404
        "bizbiz3‘; begin declare @var varchar(8000) set @var=’:’ select @ var=@var+’+login+’/’+password+’ ‘ from users where login > @var select @var as var into temp end -- "| "658428" | "SQL Injection" | 404
        "mooh02"  | "445078‘/**/OR/**/1/**/=/**/1" | "SQL Injection" | 403
        "mooh02"  | "445078‘ and 1 in (select var from temp)--" | "SQL Injection" | 403
        "mooh02"  | "445078‘ union select 1,load_file(‘/etc/passwd’),1,1,1; 1;(load_file(char(47,101,116,99,47,112,97,115,115,119,100))),1,1,1;" | "SQL Injection" | 403
        "mooh02"  | "445078‘ and 1=( if((load_file(char(110,46,101,120,116))<>ch ar(39,39)),1,0));" | "SQL Injection" | 403
        "mooh02"  | "445078 Password:*/=1--"   | "SQL Injection" | 403
    }
}
