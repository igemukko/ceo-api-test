package com.baeminceo.api.test.log

import com.baeminceo.api.test.utils.TestInit
import io.restassured.RestAssured
import io.restassured.path.json.JsonPath
import spock.lang.Unroll

import static com.baeminceo.api.test.utils.UtilMethod.generateStringFromResource
import static com.baeminceo.api.test.utils.UtilMethod.getMyCookies
import static io.restassured.RestAssured.given


class PutSuperlogSoldout extends TestInit{
    @Unroll
    def "#EXPLAIN 재료소진 로그 추가"() {

        given:
        RestAssured.basePath = "/v1/superlog/soldout"

        requestDto.put("itemGroupName", "치킨")
        requestDto.put("itemName", "재료소진 테스트용")
        requestDto.put("itemDueDate","2017-09-26" )
        requestDto.put("memId",mem_id)
        requestDto.put("shopNo",shop_no)

        when:
        response = given()
                .log().all()
                .header("Content-Type","application/json")
                .cookies(getMyCookies("mooh02", "a1111111"))
                .body(requestDto)
                .when().put()

        response.prettyPrint()
        jsonPath = new JsonPath(response.getBody().asString())

        then:
        println(EXPLAIN + "조회 결과:" + jsonPath.getJsonObject("statusCode")+"("+response.getStatusCode()+")")
        response.getStatusCode() == result


        where:
        mem_id    | shop_no  |  EXPLAIN           | result
        "mooh02"  | "658428" | "정상 아이디"         | 200
        "bizbiz3" | "658428" | "다른 멤버아이디"      | 403
        "mooh02"  | "445078" | "다른 업주의 업소"     | 403
        "bizbiz3‘OR2BETWEEN1and3"                          | "658428" | "SQL Injection" | 404
        "bizbiz3‘ or username like char(37);"              | "658428" | "SQL Injection" | 404
        "bizbiz3‘ union select * from users where login = char(114,111,111,116);" | "658428" | "SQL Injection" | 404
        "bizbiz3‘or1/*"                                    | "658428" | "SQL Injection" | 404
        "bizbiz3‘ union select"                            | "658428" | "SQL Injection" | 404
        "mooh02"  | "bizbiz3 Password:*/=1--"                          | "SQL Injection" | 403
        "mooh02"  | "bizbiz3 UNI/**/ON SEL/**/ECT"                     | "SQL Injection" | 403
        "mooh02"  | "bizbiz3‘; EXECUTE IMMEDIATE ‘SEL’ || ‘ECT US’ || ‘ER’" | "SQL Injection" | 403
        "mooh02"  | "bizbiz3‘; EXEC (‘SEL’ + ‘ECT US’ + ‘ER’)"        | "SQL Injection" | 403
        "mooh02"  | "bizbiz3‘/**/OR/**/1/**/=/**/1"                   | "SQL Injection" | 403
    }
}
