package com.baeminceo.api.test.shopreview

import com.baeminceo.api.test.utils.TestInit
import io.restassured.RestAssured
import io.restassured.http.ContentType
import io.restassured.path.json.JsonPath
import spock.lang.Unroll

import static com.baeminceo.api.test.utils.UtilMethod.generateStringFromResource
import static com.baeminceo.api.test.utils.UtilMethod.getJsonData
import static com.baeminceo.api.test.utils.UtilMethod.getMyCookies
import static io.restassured.RestAssured.given

class AddReviewReply extends TestInit {

    def setup() {
        RestAssured.basePath = "/v1/reviews/{shopReviewSeq}/reply"
    }

    /*
    * 사용자의 업소에 등록된 리뷰의 댓글을 등록한다.
    * 원 api: 헤더에 x-mem-no를 포함해서 api 요청
    * 수정 api: 업주/업소 정보가 변조되어 파라미터로 전송되었을 경우 front 에서 403 에러
    * 본 api는 url 파라미터로 전송하는 정보는 리뷰번호임
    * 소유 리뷰인지 여부에 대해 유효성 처리해야 함
    */
    @Unroll
    def "[CEO] ADD 리뷰댓글 - #EXPLAIN" () {
        given: "헤더 정보 설정"
        requestHeader.put("Content-Type", ContentType.JSON)

        requestBody = generateStringFromResource(JSONPATH)

        when:
        response = given()
                .log().all()
                .cookies(getMyCookies(LOGINID, LOGINPW))
                .headers(requestHeader)
                .pathParams("shopReviewSeq", SHOPREVIEWSEQ)
                .body(requestBody)
                .put()

        response.prettyPrint()
        jsonPath = new JsonPath(response.getBody().asString())

        then:
        response.getStatusCode() == EXPECTRE1
        getJsonData("statusCode", jsonPath) == null ? EXPECTRE2 : getJsonData("statusCode", jsonPath).contains(EXPECTRE2) == true

        where:
        LOGINID     |   LOGINPW     |   JSONPATH                    |   SHOPREVIEWSEQ                                   ||  EXPECTRE1   |   EXPECTRE2   |   EXPLAIN
        "lee00m"    |   "111111"    |   "add_review_reply.json"     |   "15093813"                                      ||  200         |   "SUCCESS"   |   "소유 리뷰"
        "lee00m"    |   "111111"    |   "add_review_reply.json"     |   "15093808"                                      ||  400         |   "Invalid"   |   "미소유 리뷰"
        "lee00m"    |   "111111"    |   "add_review_reply.json"     |   "15093819"                                      ||  400         |   "Invalid"   |   "없는 리뷰"
        "lee00m"    |   "111111"    |   "add_review_reply.json"     |   "15093813"                                      ||  400         |   "Invalid"   |   "삭제된 리뷰" //- 삭제된 리뷰에 대해 댓글은 성공함, 처리 여부에 대해 추후 결정되어야 함
        "lee00m"    |   "111111"    |   "add_review_reply.json"     |   " "                                             ||  404         |   true        |   "유효하지 않은 리뷰(공백)"
        'lee00m'    |   "111111"    |   "add_review_reply.json"     |   "abcedfghijklmnopqrstuvwxyz12345678"            ||  404         |   true        |   "유효하지 않은 리뷰(문자+숫자)"
        "lee00m"    |   "111111"    |   "add_review_reply.json"     |   "15093808 ‘||(elt(-3+5,bin(15),ord(10),hex(char(45))))"  ||  404         |   true        |   "sql injection"
        "lee00m"    |   "111111"    |   "add_review_reply.json"     |   "15093808 ||6"                                           ||  404         |   true        |   "sql injection"
        "lee00m"    |   "111111"    |   "add_review_reply.json"     |   "15093808 ‘||’6"                                         ||  404         |   true        |   "sql injection"
        "lee00m"    |   "111111"    |   "add_review_reply.json"     |   "15093808 (||6)"                                         ||  404         |   true        |   "sql injection"
        "lee00m"    |   "111111"    |   "add_review_reply.json"     |   "15093808 ‘ OR 1=1--"                                    ||  404         |   true        |   "sql injection"
        "lee00m"    |   "111111"    |   "add_review_reply.json"     |   "15093808 ; OR ‘1’=’1’"                                  ||  404         |   true        |   "sql injection"

    }
}
