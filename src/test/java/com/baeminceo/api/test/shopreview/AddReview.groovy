package com.baeminceo.api.test.shopreview

import com.baeminceo.api.test.utils.TestInit
import io.restassured.RestAssured
import io.restassured.http.ContentType
import io.restassured.path.json.JsonPath
import spock.lang.Unroll

import static com.baeminceo.api.test.utils.UtilMethod.generateStringFromResource
import static com.baeminceo.api.test.utils.UtilMethod.getJsonData
import static com.baeminceo.api.test.utils.UtilMethod.getMyCookies
import static com.baeminceo.api.test.utils.UtilMethod.updateToResource
import static io.restassured.RestAssured.given

class AddReview extends TestInit {

    def setup() {
        RestAssured.basePath = "/v1/reviews"
    }

    /*
    * 사용자의 업소에 리뷰를 등록한다.
    * 원 api: 헤더에 x-mem-no를 포함해서 api 요청
    * 수정 api: 업주/업소 정보가 변조되어 파라미터로 전송되었을 경우 front 에서 403 에러
    * 본 api는 url 파라미터로 전송하는 정보는 없으며 JSON DATA로 업소 번호 및 리뷰 정보를 포함시킴
    * 소유 업소 여부에 대해 유효성은 B2B에서 처리해야 함
    */
    @Unroll
    def "[CEO] ADD 리뷰 - #EXPLAIN" () {
        given: "헤더 정보 설정"
        requestHeader.put("Content-Type", ContentType.JSON)

        updateToResource(JSONPATH, "shopNo", SHOPNO)
        requestBody = generateStringFromResource(JSONPATH)

        when:
        response = given()
                .log().all()
                .cookies(getMyCookies(LOGINID, LOGINPW))
                .headers(requestHeader)
                .body(requestBody)
                .put()

        response.prettyPrint()
        jsonPath = new JsonPath(response.getBody().asString())

        then:
        response.getStatusCode() == EXPECTRE1
        getJsonData("statusCode", jsonPath) == null ? EXPECTRE2 : getJsonData("statusCode", jsonPath).contains(EXPECTRE2) == true

        where:
        LOGINID     |   LOGINPW     |   JSONPATH              |   SHOPNO                                ||  EXPECTRE1   |   EXPECTRE2   |   EXPLAIN
        "lee00m"    |   "111111"    |   "add_review.json"     |   "445078"                              ||  200         |   "SUCCESS"   |   "소유 업소"
        "lee00m"    |   "111111"    |   "add_review.json"     |   "658428"                              ||  400         |   "Invalid"   |   "미소유 업소"
        "lee00m"    |   "111111"    |   "add_review.json"     |   "000000"                              ||  400         |   "Invalid"   |   "없는 업소"
        "lee00m"    |   "111111"    |   "add_review.json"     |   " "                                   ||  400         |   "Invalid"   |   "유효하지 않은 업소 조회(공백)"
        'lee00m'    |   "111111"    |   "add_review.json"     |   "658428 OR 1=1"                       ||  403         |   "forbidden"   |   "sql injection"
        'lee00m'    |   "111111"    |   "add_review.json"     |   "658428 ‘ OR ‘1’=’1"                  ||  403         |   "forbidden"   |   "sql injection"
        'lee00m'    |   "111111"    |   "add_review.json"     |   "658428 ; OR ‘1’=’1’"                 ||  403         |   "forbidden"   |   "sql injection"
        'lee00m'    |   "111111"    |   "add_review.json"     |   "658428 %22+or+isnull%281%2F0%29+%2F*"||  403         |   "forbidden"   |   "sql injection"
        'lee00m'    |   "111111"    |   "add_review.json"     |   "658428 %27+OR+%277659%27%3D%277659"  ||  403         |   "forbidden"   |   "sql injection"

    }
}
