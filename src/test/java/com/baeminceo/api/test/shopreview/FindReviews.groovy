package com.baeminceo.api.test.shopreview

import com.baeminceo.api.test.utils.TestInit
import io.restassured.RestAssured
import io.restassured.http.ContentType
import io.restassured.path.json.JsonPath
import spock.lang.Unroll

import static com.baeminceo.api.test.utils.UtilMethod.generateStringFromResource
import static com.baeminceo.api.test.utils.UtilMethod.getJsonData
import static com.baeminceo.api.test.utils.UtilMethod.getMyCookies
import static io.restassured.RestAssured.given

class FindReviews extends TestInit {

    def setup() {
        RestAssured.basePath = "/v1/reviews"
    }

    /*
    * 특정 업소의 리뷰, 댓글, 리뷰 첨부 이미지를 조회한다.
    * 원 api: 헤더에 x-mem-no 및 x-shopowner-id를 포함해서 api 요청
    * 수정 api: 업주/업소 정보가 변조되어 파라미터로 전송되었을 경우 front 에서 403 에러
    * 본 api는 파라미터로 전송하는 정보는 업소번호 및 업주번호를 optional 하게 포함시킴
    * 업주 및 업소의 소유 리뷰인지 여뷰에 대해 Front에서 유효성 처리해야 함
    */
    @Unroll
    def "[CEO] FIND 리뷰 - #EXPLAIN" () {
        given: "헤더 정보 설정"
        requestHeader.put("Content-Type", ContentType.JSON)
        requestDto.put("shopNo", SHOPNO)
        requestDto.put("shopOwnerNo", SHOPOWNERNO)


        when:
        response = given()
                .log().all()
                .cookies(getMyCookies(LOGINID, LOGINPW))
                .headers(requestHeader)
                .queryParams("shopNo", SHOPNO)
                .get()

        response.prettyPrint()
        jsonPath = new JsonPath(response.getBody().asString())

        then:
        response.getStatusCode() == EXPECTRE1
        getJsonData("statusCode", jsonPath).contains(EXPECTRE2) == true

         where:
         LOGINID     |   LOGINPW     |   SHOPNO                              |   SHOPOWNERNO     ||  EXPECTRE1   |   EXPECTRE2       |   EXPLAIN
         "lee00m"    |   "111111"    |   "all"                               |   null            ||  200         |   "SUCCESS"       |   "모든 업소"
         "lee00m"    |   "111111"    |   "445078"                            |   null            ||  200         |  "SUCCESS"        |   "소유 업소"
         "lee00m"    |   "111111"    |   "123456"                            |   null            ||  403         |   "forbidden"     |   "없는 업소"
         "lee00m"    |   "111111"    |   " "                                 |   null            ||  403         |   "forbidden"     |   "유효하지 않은 업소 조회(공백)"
         "lee00m"    |   "111111"    |   "abcedfghijklmnopqrstuvwxyz12345678"|   null            ||  403         |   "forbidden"     |   "유효하지 않은 업소 조회(문자+숫자)"
//         "lee00m"    |   "111111"    |   "all"                               |   "201203220077"  ||  403         |   "forbidden"     |   "다른 업주 번호"
         "lee00m"    |   "111111"    |   "‘ union all select @@version--"    |   "201203220077"  ||  403         |   "forbidden"     |   "sql injection"
         "lee00m"    |   "111111"    |   "‘ OR ‘unusual’ = ‘unusual’"        |   "201203220077"  ||  403         |   "forbidden"     |   "sql injection"
         "lee00m"    |   "111111"    |   "‘ OR ‘something’ = ‘some’+’thing’" |   "201203220077"  ||  403         |   "forbidden"     |   "sql injection"
         "lee00m"    |   "111111"    |   "‘ OR ‘text’ = N’text’"             |   "201203220077"  ||  403         |   "forbidden"     |   "sql injection"
         "lee00m"    |   "111111"    |   "‘ OR ‘something’ like ‘some%’"     |   "201203220077"  ||  403         |   "forbidden"     |   "sql injection"
         "lee00m"    |   "111111"    |   "; OR ‘1’=’1’"                      |   "201203220077"  ||  403         |   "forbidden"     |   "sql injection"

    }

//    /*
//    * 업주 번호가 파라미터로 들어가는지 확인 필요
//    * 사용자의 업소에 등록된 리뷰를 조회한다.
//    * 원 api: 헤더에 x-mem-no 및 x-shopowner-id를 포함해서 api 요청
//    * 수정 api: 업주/업소 정보가 변조되어 파라미터로 전송되었을 경우 front 에서 403 에러
//    * 본 api는 파라미터로 전송하는 정보는 업소번호 및 업주번호를 optional 하게 포함시킴
//    * 업주 및 업소의 소유 리뷰인지 여뷰에 대해 유효성 처리해야 함
//    */
//    @Unroll
//    def "[CEO] FIND 리뷰 - #EXPLAIN" () {
//        given: "헤더 정보 설정"
//        requestHeader.put("Content-Type", ContentType.JSON)
//
//        requestDto.put("shopOwnerNo", SHOPOWNERNO)
//
//        when:
//        response = given()
//                .log().all()
//                .cookies(getMyCookies(LOGINID, LOGINPW))
//                .headers(requestHeader)
//                .queryParams(requestDto)
//                .get()
//
//        response.prettyPrint()
//        jsonPath = new JsonPath(response.getBody().asString())
//
//        then:
//        EXPLAIN == "정상 업주 번호" ? getJsonData("statusCode", jsonPath).contains("SUCCESS") == true : response.getStatusCode() == 403
//
//        where:
//        LOGINID     |   LOGINPW     |   SHOPOWNERNO                             ||  EXPLAIN
//        "lee00m"    |   "111111"    |   "201203220076"                          ||  "정상 업주번호"
//        "lee00m"    |   "111111"    |   "201709130001"                          ||  "다른 업주 번호"
//        "lee00m"    |   "111111"    |   " "                                     ||  "유효하지 않은 업주 번호(공백)"
//        'lee00m'    |   "111111"    |   "abcedfghijklmnopqrstuvwxyz12345678"    ||  "유효하지 않은 업소 조회(문자+숫자)"
//
//    }

}
