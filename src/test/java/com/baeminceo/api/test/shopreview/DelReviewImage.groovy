package com.baeminceo.api.test.shopreview

import com.baeminceo.api.test.utils.TestInit
import io.restassured.RestAssured
import io.restassured.http.ContentType
import io.restassured.path.json.JsonPath
import spock.lang.Unroll

import static com.baeminceo.api.test.utils.UtilMethod.getJsonData
import static com.baeminceo.api.test.utils.UtilMethod.getMyCookies
import static io.restassured.RestAssured.given

class DelReviewImage extends TestInit {

    def setup() {
        RestAssured.basePath = "/v1/reviews/{shopReviewSeq}/images/{shopReviewImageSeq}"
    }

    /*
    * 사용자의 업소에 등록된 리뷰의 이미지를 삭제한다.
    * 원 api: 헤더에 x-mem-no를 포함해서 api 요청
    * 수정 api: 업주/업소 정보가 변조되어 파라미터로 전송되었을 경우 front 에서 403 에러
    * 본 api는 파라미터로 전송하는 정보는 리뷰번호 및 리뷰 이미지 번호를 포함시킴
    * 소유 리뷰인지 여부에 대해 유효성 처리해야 함
    */
    @Unroll
    def "[CEO] DEL 리뷰 이미지삭제 - #EXPLAIN" () {
        given: "헤더 정보 설정"
        requestHeader.put("Content-Type", ContentType.JSON)

        requestDto.put("shopReviewSeq", SHOPREVIEWSEQ)
        requestDto.put("shopReviewImageSeq", SHOPREVIEWIMGSEQ)

        when:
        response = given()
                .log().all()
                .cookies(getMyCookies(LOGINID, LOGINPW))
                .headers(requestHeader)
                .pathParams(requestDto)
                .delete()

        response.prettyPrint()
        jsonPath = new JsonPath(response.getBody().asString())

        then:
        response.getStatusCode() == EXPECTRE1
        getJsonData("statusCode", jsonPath) == null ? EXPECTRE2 : getJsonData("statusCode", jsonPath).contains(EXPECTRE2) == true

        where:
        LOGINID     |   LOGINPW     |   SHOPREVIEWSEQ                       |   SHOPREVIEWIMGSEQ                                                    ||  EXPECTRE1   |   EXPECTRE2   |   EXPLAIN
        "lee00m"    |   "111111"    |   "15093813"                          |   1                                                                   ||  200         |  "SUCCESS"   |   "소유 리뷰"
        "lee00m"    |   "111111"    |   "15093808"                          |   1                                                                   ||  400         |   "Invalid"   |   "미소유 리뷰"
        "lee00m"    |   "111111"    |   "15093812"                          |   1                                                                   ||  400         |   "Invalid"   |   "이미 삭제된 리뷰 및 리뷰 이미지"
        "lee00m"    |   "111111"    |   " "                                 |   1                                                                   ||  404         |   true        |   "유효하지 않은 리뷰 seq(공백)"
        'lee00m'    |   "111111"    |   "abcedfghijklmnopqrstuvwxyz12345678"|   1                                                                   ||  404         |   true        |   "유효하지 않은 리뷰 seq(문자+숫자)"
        "lee00m"    |   "111111"    |   "15093813"                          |   123456789                                                           ||  400         |   "Invalid"   |   "없는 이미지 번호"
        "lee00m"    |   "111111"    |   "15093813"                          |   "abcedfghijklmnopqrstuvwxyz12345678"                                ||  404         |   true        |   "유효하지 않은 이미지 번호"
        "lee00m"    |   "111111"    |   "15093813"                          |   " "                                                                 ||  404         |   true        |   "유효하지 않은 이미지 번호(공백)"
        "lee00m"    |   "111111"    |   "15093813"                          |   "7 ‘ union select * from users where login = char(114,111,111,116);"  ||  404         |   true        |   "sql injection"
        "lee00m"    |   "111111"    |   "15093813"                          |   "7 ‘ union select"                                                    ||  404         |   true        |   "sql injection"
        "lee00m"    |   "111111"    |   "15093813"                          |   "7 Password:*/=1--"                                                   ||  404         |   true        |   "sql injection"
        "lee00m"    |   "111111"    |   "15093813"                          |   "7 UNI/**/ON SEL/**/ECT"                                              ||  404         |   true        |   "sql injection"
        "lee00m"    |   "111111"    |   "15093813"                          |   "7 ‘; EXECUTE IMMEDIATE ‘SEL’ || ‘ECT US’ || ‘ER’"                    ||  404         |   true        |   "sql injection"
        "lee00m"    |   "111111"    |   "15093813"                          |   "7 ; OR ‘1’=’1’"                                                     ||  404         |   true        |   "sql injection"
        "lee00m"    |   "111111"    |   "15093808‘; EXEC (‘SEL’ + ‘ECT US’ + ‘ER’)" |   1                                                                   ||  404         |   true        |   "sql injection"
        "lee00m"    |   "111111"    |   "15093808‘/**/OR/**/1/**/=/**/1"            |   1                                                                   ||  404         |   true        |   "sql injection"
        "lee00m"    |   "111111"    |   "15093808‘or1/* "                            |   1                                                                   ||  404         |   true        |   "sql injection"
        "lee00m"    |   "111111"    |   "15093808+or+isnull%281%2F0%29+%2F*"        |   1                                                                   ||  404         |   true        |   "sql injection"
        "lee00m"    |   "111111"    |   "15093808%27+OR+%277659%27%3D%277659"       |   1                                                                   ||  404         |   true        |   "sql injection"
        "lee00m"    |   "111111"    |   "15093808; OR ‘1’=’1’ "                     |   1                                                                   ||  404         |   true        |   "sql injection"

    }
}
