package com.baeminceo.api.test.shopreview

import com.baeminceo.api.test.utils.TestInit
import io.restassured.RestAssured
import io.restassured.http.ContentType
import io.restassured.path.json.JsonPath
import spock.lang.Unroll

import static com.baeminceo.api.test.utils.UtilMethod.getJsonData
import static com.baeminceo.api.test.utils.UtilMethod.getMyCookies
import static io.restassured.RestAssured.given

class DelReview extends TestInit {

    def setup() {
        RestAssured.basePath = "/v1/reviews"
    }

    /*
    * 업소의 리뷰를 삭제한다.
    * 원 api: 헤더에 x-mem-no를 포함해서 api 요청
    * 수정 api: 업주/업소 정보가 변조되어 파라미터로 전송되었을 경우 front 에서 403 에러
    * 본 api는 url 파라미터로 전송하는 정보는 리뷰번호 및 editor를 포함시킴
    * 소유 리뷰인지 여부에 대해 B2B 서버에서 유효성 처리해야 함
    */
    @Unroll
    def "[CEO] DEL 리뷰- #EXPLAIN" () {

        given: "헤더 정보 설정"
        requestHeader.put("Content-Type", ContentType.JSON)

        when:
        response = given()
                .log().all()
                .cookies(getMyCookies(LOGINID, LOGINPW))
                .headers(requestHeader)
                .queryParams("editor", EDITOR)
                .delete(SHOPREVIEWSEQ)

        response.prettyPrint()
        jsonPath = new JsonPath(response.getBody().asString())

        then:
        response.getStatusCode() == EXPECTRE1
        getJsonData("statusCode", jsonPath) == null ? EXPECTRE2 : getJsonData("statusCode", jsonPath).contains(EXPECTRE2) == true

        where:
        LOGINID     |   LOGINPW     |   SHOPREVIEWSEQ                                                                                                                                                   |   EDITOR      ||  EXPECTRE1   |   EXPECTRE2   |   EXPLAIN
        "lee00m"    |   "111111"    |   "15093813"                                                                                                                                                      |   "lee00m"    ||  200         |   "SUCCESS"   |   "소유 리뷰"
        "lee00m"    |   "111111"    |   "15093808"                                                                                                                                                      |   "hkkang"    ||  400         |   "Invalid"   |   "미소유 리뷰"
        "lee00m"    |   "111111"    |   "15093819"                                                                                                                                                      |   "hkkang"    ||  400         |   "Invalid"   |   "없는 리뷰"
        "lee00m"    |   "111111"    |   " "                                                                                                                                                             |   "hkkang"    ||  405         |   true        |   "유효하지 않은 리뷰(공백)"
        "lee00m"    |   "111111"    |   "abcedfghijklmnopqrstuvwxyz12345678"                                                                                                                            |   "hkkang"    ||  404         |   true        |   "유효하지 않은 리뷰(문자+숫자)"
        'lee00m'    |   "111111"    |   "15093813"                                                                                                                                                      |   "hkkang"    ||  400         |   "Invalid"   |   "다른 소유자 ID" //- 삭제 시 editor는 헤더 정보 확인 파라미터 정보는 로그를 남길 때 사용
        "lee00m"    |   "111111"    |   "15093813%22+or+isnull%281%2F0%29+%2F*"                                                                                                                                  |   "lee00m"    ||  404         |   true         |   "sql injection"
        "lee00m"    |   "111111"    |   "15093813 ‘; begin declare @var varchar(8000) set @var=’:’ select @ var=@var+’+login+’/’+password+’ ‘ from users where login > @var select @var as var into temp end -- "|   "lee00m"    ||  404         |   true         |   "sql injection"
        "lee00m"    |   "111111"    |   "15093813 ‘ and 1 in (select var from temp)--"                                                                                                                           |   "lee00m"    ||  404         |   true         |   "sql injection"
        "lee00m"    |   "111111"    |   "15093813 ‘ union select 1,load_file(‘/etc/passwd’),1,1,1; 1;(load_file(char(47,101,116,99,47,112,97,115,115,119,100))),1,1,1;"                                          |   "lee00m"    ||  404         |   true         |   "sql injection"
        "lee00m"    |   "111111"    |   "15093813 ‘ and 1=( if((load_file(char(110,46,101,120,116))<>ch ar(39,39)),1,0));"                                                                                       |   "lee00m"    ||  404         |   true         |   "sql injection"
        "lee00m"    |   "111111"    |   "15093808; OR ‘1’=’1’"                                                                                                                                                   |   "lee00m"    ||  400         |   "Invalid"    |   "sql injection"


    }
}
