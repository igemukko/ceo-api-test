package com.baeminceo.api.test.shopreview

import com.baeminceo.api.test.utils.TestInit
import io.restassured.RestAssured
import io.restassured.http.ContentType
import io.restassured.path.json.JsonPath
import spock.lang.Unroll

import static com.baeminceo.api.test.utils.UtilMethod.getJsonData
import static com.baeminceo.api.test.utils.UtilMethod.getMyCookies
import static io.restassured.RestAssured.given

class SetNotice extends TestInit {

    def setup() {
        RestAssured.basePath = "/v1/reviews/{shopReviewSeq}/notice"
    }

    /*
    * 리뷰공지를 설정한다.
    * 원 api: 헤더에 x-mem-no를 포함해서 api 요청
    * 수정 api: 업주/업소 정보가 변조되어 파라미터로 전송되었을 경우 front 에서 403 에러
    * 본 api는 파라미터로 전송하는 정보는 공지설정 여부 및 리뷰 번호임
    * 업주의 소유 리뷰인지 여뷰에 대해 B2B 서버에서 유효성 처리해야 함
    */

    @Unroll
    def "[CEO] SET 리뷰공지 - #EXPLAIN"() {

        given: "헤더 정보 설정"
        requestHeader.put("Content-Type", ContentType.JSON)

        requestDto.put("enable", ENABLE)

        when:
        response = given()
                .log().all()
                .cookies(getMyCookies(LOGINID, LOGINPW))
                .headers(requestHeader)
                .pathParam("shopReviewSeq", SHOPREVIEWSEQ)
                .body(requestDto)
                .post()

        response.prettyPrint()
        jsonPath = new JsonPath(response.getBody().asString())

        then:
        response.getStatusCode() == EXPECTRE1
        getJsonData("statusCode", jsonPath) == null ? EXPECTRE2 : getJsonData("statusCode", jsonPath).contains(EXPECTRE2) == true

        where:
        LOGINID  | LOGINPW  | ENABLE | SHOPREVIEWSEQ                                                                                     || EXPECTRE1 | EXPECTRE2 | EXPLAIN
        "lee00m"    |   "111111"    |   true    |   "15093814"                          ||  200         |   "SUCCESS"   |   "소유 리뷰"
        "lee00m"    |   "111111"    |   true    |   "15093808"                          ||  400         |   "Invalid"   |   "미소유 리뷰"
        "lee00m"    |   "111111"    |   true    |   "15093812"                          ||  400         |   "Invalid"   |   "삭제된 리뷰"
        "lee00m"    |   "111111"    |   true    |   "15093819"                          ||  400         |   "Invalid"   |   "없는 리뷰"
        "lee00m"    |   "111111"    |   true    |   " "                                 ||  404         |   true        |   "유효하지 않은 리뷰(공백)"
        'lee00m'    |   "111111"    |   true    |   "abcedfghijklmnopqrstuvwxyz12345678"||  404         |   true        |   "유효하지 않은 리뷰(문자+숫자)"
        'lee00m' | "111111" | true   | "15093808) UNION SELECT%20*%20FROM%20INFORMATION_SCHEMA. TABLES;"                                           || 404       | true      | "sql injection"
        'lee00m' | "111111" | true   | "15093808‘ having 1=1--"                                                                                    || 404       | true      | "sql injection"
        'lee00m' | "111111" | true   | "15093808‘ group by userid having 1=1--"                                                                    || 404       | true      | "sql injection"
        'lee00m' | "111111" | true   | "15093808‘ SELECT name FROM syscolumns WHERE id = (SELECT id FROM sysobjects WHERE name = tablename’)--"    || 404       | true      | "sql injection"
        'lee00m' | "111111" | true   | "15093808‘ or 1 in (select @@version)--"                                                                    || 404       | true      | "sql injection"
        'lee00m' | "111111" | true   | "15093808‘ OR ‘1’=’1"                                                                                       || 404       | true      | "sql injection"
    }

}